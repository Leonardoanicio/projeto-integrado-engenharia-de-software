import {
  CognitoUserPool,
  ICognitoUserPoolData,
  CognitoUser,
  ICognitoUserData,
  CognitoUserSession,
  AuthenticationDetails,
  IAuthenticationDetailsData,
  ISignUpResult,
  CognitoUserAttribute,
  ICognitoUserAttributeData
} from 'amazon-cognito-identity-js';
import { UserNotFound } from '../errors/UserNotFound';

type authUserType = {
  accessToken?: string;
  token?: string;
  refreshToken?: string;
  cognitoUserSession?: CognitoUserSession;
  error?: string;
};

const getUserPool = () => {
  const userPoolCredentials: ICognitoUserPoolData = {
    UserPoolId: process.env.REACT_APP_COGNITO_POOL_ID || '',
    ClientId: process.env.REACT_APP_COGNITO_CLIENT_ID || ''
  };
  const userPool = new CognitoUserPool(userPoolCredentials);

  return userPool;
};

// Confirmação
export const cognitoConfirmCode = async (email: string, confirmationCode: string) => {
  const userPool = getUserPool();
  const userData: ICognitoUserData = {
    Username: email,
    Pool: userPool
  };
  const cognitoUser = new CognitoUser(userData);
  const cognitoResponse = new Promise<string>((resolve, reject) => {
    cognitoUser.confirmRegistration(confirmationCode, true, function (err, result: string) {
      if (err) reject(err);
      resolve(result);
    });
  });

  return cognitoResponse;
};

export const cognitoSendVerificationCode = async (email: string) => {
  const userPool = getUserPool();
  const userData: ICognitoUserData = {
    Username: email,
    Pool: userPool
  };
  const cognitoUser = new CognitoUser(userData);
  const cognitoResponse = new Promise<string>((resolve, reject) => {
    cognitoUser.resendConfirmationCode(function (err, result: string) {
      if (err) reject(err);
      resolve(result);
    });
  });

  return cognitoResponse;
};

// Cadastro
export const cognitoSignUp = async (
  email: string,
  password: string,
  userType: string
): Promise<ISignUpResult | undefined> => {
  const userPool = getUserPool();
  const userTypeAttribute: ICognitoUserAttributeData = {
    Name: 'custom:user_type',
    Value: userType
  };
  const userAttribute = new CognitoUserAttribute(userTypeAttribute);
  const cognitoUserRequest = new Promise<ISignUpResult>((resolve, reject) => {
    userPool.signUp(email, password, [userAttribute], [], function (err, result) {
      if (result) resolve(result);
      if (err) reject(err);
      reject(undefined);
    });
  });

  return cognitoUserRequest;
};

// Login
export const cognitoSignIn = async (email: string, password: string) => {
  const userPool = getUserPool();
  const userData: ICognitoUserData = {
    Username: email,
    Pool: userPool
  };
  const cognitoUser = new CognitoUser(userData);

  const authData: IAuthenticationDetailsData = {
    Username: email,
    Password: password
  };

  const authDetails = new AuthenticationDetails(authData);
  const cognitoAuth = new Promise<CognitoUserSession>((resolve, reject) => {
    cognitoUser.authenticateUser(authDetails, {
      onSuccess: function (result) {
        resolve(result);
      },
      onFailure: function (err) {
        reject(err);
      }
    });
  });

  const userAuth: authUserType = await cognitoAuth
    .then(async (result) => {
      const cognitoUserSession = await result;
      const token = await result.getIdToken().getJwtToken();
      const refreshToken = await result.getRefreshToken().getToken();
      const accessToken = await result.getAccessToken().getJwtToken();
      return {
        cognitoUserSession,
        token,
        refreshToken,
        accessToken
      };
    })
    .catch((err) => {
      return {
        error: (err as Error).message
      };
    });

  return { userAuth, cognitoUser };
};

export const cognitoGetUserType = async (cognitoUser: CognitoUser) => {
  const cognitoResponse = new Promise<CognitoUserAttribute[] | undefined>((resolve, reject) => {
    cognitoUser.getUserAttributes(function (err, result) {
      if (err) reject(err);
      resolve(result);
    });
  });
  const userAttributes = await cognitoResponse;
  if (!userAttributes) {
    throw new UserNotFound();
  }

  const userType = userAttributes.filter((attr) => attr.Name === 'custom:user_type');
  if (userType.length === 0) {
    throw new UserNotFound();
  }

  return userType[0].Value;
};
