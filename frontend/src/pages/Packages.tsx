import React from 'react';

import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { NavigationBar } from '../components/Navigation/NavigationBar';
import { PacksView } from '../views/Packs';

export const Pacotes = () => {
  return (
    <div className="App">
      <Header />
      <div className="PageWithNavigator">
        <NavigationBar />
        <PacksView />
      </div>
      <Footer />
    </div>
  );
};
