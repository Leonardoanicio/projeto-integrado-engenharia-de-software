import React from 'react';

import '../styles/About.css';

import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { Link } from 'react-router-dom';

export const AboutPage = () => (
  <div className="App">
    <Header />
    <div className="About">
      <h1>Sobre</h1>
      <div className="RowGroup">
        <div className="ColumnGroup">
          <Link className="Link" to="/sobre">
            Quem somos?
          </Link>
          <Link className="Link" to="/sobre">
            Nossos valores
          </Link>
          <Link className="Link" to="/sobre">
            Trabalhe na pingo
          </Link>
        </div>
        <div className="ColumnGroup">
          <Link className="Link" to="/sobre">
            Matérias
          </Link>
          <Link className="Link" to="/sobre">
            Aulas Online
          </Link>
          <Link className="Link" to="/sobre">
            Política de Privacidade
          </Link>
        </div>
      </div>
    </div>
    <Footer />
  </div>
);
