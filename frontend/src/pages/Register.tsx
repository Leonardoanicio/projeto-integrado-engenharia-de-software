import React from 'react';

import { RegisterForm } from '../forms/Register/RegisterForm';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';

export const RegisterPage = () => (
  <div className="App">
    <Header />
    <RegisterForm />
    <Footer />
  </div>
);
