import React from 'react';

import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { AgendaView } from '../views/Agenda/Agenda';
import { NavigationBar } from '../components/Navigation/NavigationBar';

export const Agenda = () => {
  return (
    <div className="App">
      <Header />
      <div className="PageWithNavigator">
        <NavigationBar />
        <AgendaView />
      </div>
      <Footer />
    </div>
  );
};
