import React from 'react';

import { PhotoForm } from '../forms/Photo/PhotoForm';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { NavigationBar } from '../components/Navigation/NavigationBar';

export const Photo = () => (
  <div className="App">
    <Header />
    <div className="PageWithNavigator">
      <NavigationBar />
      <PhotoForm />
    </div>
    <Footer />
  </div>
);
