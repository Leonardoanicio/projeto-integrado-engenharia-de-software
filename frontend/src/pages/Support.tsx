import React from 'react';
import '../styles/Suport.css';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { Link } from 'react-router-dom';

export const SuportPage = () => (
  <div className="App">
    <Header />
    <div className="Suport">
      <h1>Suporte</h1>
      <div className="RowGroup">
        <div className="ColumnGroup">
          <Link className="Link" to="/suporte">
            Fale conosco
          </Link>
          <Link className="Link" to="/suporte">
            Contato
          </Link>
        </div>
      </div>
    </div>
    <Footer />
  </div>
);
