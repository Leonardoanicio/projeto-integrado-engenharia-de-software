import React from 'react';

import { CashForm } from '../forms/Cash/CashForm';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { NavigationBar } from '../components/Navigation/NavigationBar';

export const Cash = () => (
  <div className="App">
    <Header />
    <div className="PageWithNavigator">
      <NavigationBar />
      <CashForm />
    </div>
    <Footer />
  </div>
);
