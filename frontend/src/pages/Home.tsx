import '../styles/Home.css';
import React, { useState } from 'react';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { Card } from '../components/Cards/Card';

import { InputAdornment, OutlinedInput, Button } from '@mui/material';

import card1 from '../images/card1.png';
import card2 from '../images/card2.png';
import card3 from '../images/card3.png';

import { SearchButton } from '../components/Buttons/Search/SearchButton';

export const HomePage = () => {
  const [search, setSearch] = useState('');

  return (
    <div className="App">
      <Header />
      <div className="PageOne">
        <div className="Content">
          <h1>Encontre seu professor</h1>
          <h2>Consulte perfis, pacotes e faça sua escolha:</h2>
        </div>
      </div>
      <div className="PageTwo">
        <h1>Aprenda com facilidade, sem sair de casa!</h1>
        <div className="Cards">
          <Card
            cardHeader="Pesquise"
            cardText="Escolha entre os melhores professores"
            background="#FEF4AC"
            textColor="black"
            textBackground="#FFFEFE"
            img={card1}
          />
          <Card
            cardHeader="Selecione"
            cardText="Quantos pingos cabem no seu bolso?"
            background="white"
            textColor="black"
            textBackground="#E2E2E2"
            img={card2}
          />
          <Card
            cardHeader="Agende"
            cardText="Pronto! Agora é só começar as aulas!"
            background="#FEF4AC"
            textColor="black"
            textBackground="#FFFEFE"
            img={card3}
          />
        </div>
      </div>
      <div className="PageThree">
        <h1>
          Seja um aluno <span className="Logo">pingo</span>
        </h1>
        <h2>Compre créditos para ter acesso aos pacotes e começe a aprender!</h2>
        <Button className="BotaoCadastro"> Cadastre-se </Button>
      </div>
      <div className="PageFour">
        <h1>
          Torne-se um professor <span className="Logo">pingo</span>
        </h1>
        <h2>Passe seus conhecimento adiante e aumente a sua renda!</h2>
        <Button className="BotaoCadastro"> Quero dar aulas </Button>
      </div>
      <div className="PageFive"></div>
      <Footer />
    </div>
  );
};
