import React from 'react';

import { LoginForm } from '../forms/Login/LoginForm';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';

export const LoginPage = () => {
  return (
    <div className="App">
      <Header />
      <LoginForm />
      <Footer />
    </div>
  );
};
