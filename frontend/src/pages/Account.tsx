import React from 'react';

import { AccountForm } from '../forms/Account/AccountForm';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { NavigationBar } from '../components/Navigation/NavigationBar';

export const Account = () => (
  <div className="App">
    <Header />
    <div className="PageWithNavigator">
      <NavigationBar />
      <AccountForm />
    </div>
    <Footer />
  </div>
);
