import React from 'react';

import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { DisciplinesView } from '../views/Disciplines/Disciplines';
import { NavigationBar } from '../components/Navigation/NavigationBar';

export const Disciplines = () => {
  return (
    <div className="App">
      <Header />
      <div className="PageWithNavigator">
        <NavigationBar />
        <DisciplinesView />
      </div>
      <Footer />
    </div>
  );
};
