import React from 'react';

import { ConfirmRegistration } from '../forms/ConfirmRegistration/ConfirmRegistration';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';

type PropsType = {
  email: string | undefined;
  password: string | undefined;
};

export const ConfirmRegister = ({ email, password }: PropsType) => (
  <div className="App">
    <Header />
    <ConfirmRegistration email={email} password={password} />
    <Footer />
  </div>
);
