import React from 'react';

import { ForgotPasswordForm } from '../forms/ForgotPassword/ForgotPassword';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';

export const ForgotPasswordPage = () => (
  <div className="App">
    <Header />
    <ForgotPasswordForm />
    <Footer />
  </div>
);
