import React from 'react';

import { Room } from '../views/VideoRoom/Room';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';

export const RoomPage = () => (
  <div className="App">
    <Header />
    <Room />
    <Footer />
  </div>
);
