import React from 'react';

import { ProfileForm } from '../forms/Profile/ProfileForm';
import { Header } from '../components/Headers/Header';
import { Footer } from '../components/Footer/Footer';
import { NavigationBar } from '../components/Navigation/NavigationBar';

export const Profile = () => (
  <div className="App">
    <Header />
    <div className="PageWithNavigator">
      <NavigationBar />
      <ProfileForm />
    </div>
    <Footer />
  </div>
);
