export type ReactEventType = React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
