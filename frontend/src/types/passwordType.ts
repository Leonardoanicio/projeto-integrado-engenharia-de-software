export type PasswordType = {
  sx?: { [key: string]: string };
  placeholder: string;
  password: string;
  passwordConfirmation?: string;
  warning?: JSX.Element | string;
  setPassword: React.Dispatch<React.SetStateAction<string>>;
  setPasswordConfirmation?: React.Dispatch<React.SetStateAction<string>>;
  setWarning?: React.Dispatch<React.SetStateAction<JSX.Element | string>>;
  onEnter?: () => void;
};
