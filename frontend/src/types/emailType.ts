export type EmailType = {
  placeholder?: string;
  email: string;
  emailConfirmation?: string;
  warning?: JSX.Element | string;
  setEmail: React.Dispatch<React.SetStateAction<string>>;
  setEmailConfirmation?: React.Dispatch<React.SetStateAction<string>>;
  setWarning?: React.Dispatch<React.SetStateAction<JSX.Element | string>>;
  onEnter?: () => void;
};
