import { createTheme } from '@mui/material/styles';

export const appTheme = createTheme({
  typography: {
    fontFamily: 'century-gothic, sans-serif'
  },
  components: {
    MuiFormControlLabel: {
      styleOverrides: {
        label: {
          fontSize: '1em',
        }
      }
    },
    MuiSvgIcon: {
      styleOverrides: {
        root: {
          width: '0.75em',
          height: '0.75em'
        }
      }
    },
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          fontSize: '1rem',
          fontWeight: 'bold',
          color: 'black'
        }
      }
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {
        }
      }
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          marginBottom: '0.25rem',
          fontSize: '1.25rem',
          borderRadius: '10px',
          backgroundColor: 'var(--main-input-background)',
          width: '100%',
          height: '45px'
        },
        input: {
          padding: '2%',
        }
      }
    },
    MuiSelect: {
      styleOverrides: {
        select: {
          borderRadius: '10px',
          width: '100%',
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        root: {
          fontWeight: 'bold',
          fontSize: '1rem',
          borderRadius: '10px',
          backgroundColor: 'var(--main-purple)',
          ':enabled': {
            color: 'white',
            ':hover': {
              backgroundColor: 'var(--main-purple-hover)'
            }
          },
          ':disabled': {
            backgroundColor: 'var(--main-gray)',
            color: 'white'
          }
        },
        textSizeMedium: {
          fontSize: '1rem'
        }
      }
    },
    MuiListItemButton: {
      styleOverrides: {
        root: {
          '&.Mui-selected': {
            backgroundColor: 'var(--main-purple)'
          },
          '&.Mui-selected:hover': {
            backgroundColor: 'var(--main-purple-hover)'
          }
        }
      }
    },
    MuiListItemText: {
      styleOverrides: {
        primary: {
          fontSize: '1.75rem'
        }
      }
    },
    MuiFormControl: {
      styleOverrides: {
        root: {
          width: '100%',
          alignItems: 'center'
        }
      }
    }
  }
});
