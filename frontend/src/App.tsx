import './App.css';
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';

import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { LoginPage } from './pages/Login';
import { RegisterPage } from './pages/Register';
import { AboutPage } from './pages/About';
import { SuportPage } from './pages/Support';
import { HomePage } from './pages/Home';
import { ForgotPasswordPage } from './pages/ForgotPassword';
import { Profile } from './pages/Profile';
import { Account } from './pages/Account';
import { Photo } from './pages/Photo';
import { Cash } from './pages/Cash';
import { Agenda } from './pages/Agenda';
import { Disciplines } from './pages/Disciplines';
import { ConfirmRegister } from './pages/ConfirmRegister';

import { AuthProvider } from './hooks/auth';
import { appTheme } from './themes/Theme';
import { Pacotes } from './pages/Packages';
import { RoomPage } from './pages/Room';

function App() {
  return (
    <AuthProvider>
      <ThemeProvider theme={appTheme}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/cadastro" element={<RegisterPage />} />
            <Route
              path="/confirmar"
              element={<ConfirmRegister email={undefined} password={undefined} />}
            />
            <Route path="/recuperarsenha" element={<ForgotPasswordPage />} />
            <Route path="/sobre" element={<AboutPage />} />
            <Route path="/suporte" element={<SuportPage />} />
            <Route path="/perfil" element={<Profile />} />
            <Route path="/conta" element={<Account />} />
            <Route path="/foto" element={<Photo />} />
            <Route path="/pingos" element={<Cash />} />
            <Route path="/agenda" element={<Agenda />} />
            <Route path="/disciplinas" element={<Disciplines />} />
            <Route path="/pacotes" element={<Pacotes />} />
            <Route path="/room/:roomName" element={<RoomPage />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </AuthProvider>
  );
}

export default App;
