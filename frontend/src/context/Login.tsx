import { createContext } from 'react';

export const LoginDefault = {
  email: '',
  password: ''
};
export const LoginContext = createContext(LoginDefault);
