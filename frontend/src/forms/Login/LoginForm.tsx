import './LoginForm.css';

import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { EmailInput } from '../../components/Inputs/Email/EmailInput';
import { PasswordInput } from '../../components/Inputs/Password/PasswordInput';
import { useNavigate } from 'react-router-dom';
import { LoadingButton } from '@mui/lab';
import { useAuth } from '../../hooks/auth';
import { Snack } from '../../components/Alert/Alert';

export const LoginForm = () => {
  const { signIn } = useAuth();
  const navigate = useNavigate();

  const [email, setEmail] = useState<string>('');
  const [active, setActive] = useState<boolean>(false);
  const [password, setPassword] = useState<string>('');
  const [snackbar, setSnackbar] = useState<string | undefined>('');
  const [emailWarning, setEmailWarning] = useState<string | JSX.Element>('');
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setActive(!!(password && email && emailWarning === ''));
  }, [password, emailWarning, email]);

  const handleClick = async () => {
    if (!email || !password) {
      setSnackbar('Por favor, preencha todos os campos');
      return;
    }
    setLoading(true);
    const { data, error } = await signIn({
      email,
      password
    });
    setLoading(false);
    if (data) {
      navigate('/perfil', {
        replace: true
      });
    } else if (error && error.includes('User is not confirmed.')) {
      navigate('/confirmar', {
        replace: true,
        state: { email, password }
      });
    } else {
      setSnackbar(error);
    }
  };

  return (
    <div className="LoginForm">
      <h1>Login</h1>
      <EmailInput
        email={email}
        warning={emailWarning}
        setEmail={setEmail}
        setWarning={setEmailWarning}
        onEnter={handleClick}
      />
      <PasswordInput placeholder="Senha" password={password} setPassword={setPassword} onEnter={handleClick}/>
      <LoadingButton className="LoginButton" loading={loading} disabled={!active || loading} onClick={handleClick}>
        Login
      </LoadingButton >
      <br />
      <div className="SpanInfo">
        <span className="SpanDefault">Não possui conta? </span>
        <Link to="/cadastro" className="SpanLink">
          &nbsp;Cadastre-se
        </Link>
      </div>
      <Snack open={Boolean(snackbar)} onClose={setSnackbar} severity="error" message={snackbar} />
    </div>
  );
};
