import './ConfirmRegistration.css';

import React, { useEffect, useState } from 'react';

import { useNavigate, useLocation } from 'react-router-dom';
import { AlertColor, Button, OutlinedInput } from '@mui/material';
import { useAuth } from '../../hooks/auth';
import { Snack } from '../../components/Alert/Alert';
import { cognitoConfirmCode, cognitoSendVerificationCode } from '../../services/cognito';

type PropsType = {
  email: string;
  password: string;
};

export const ConfirmRegistration = () => {
  const navigate = useNavigate();
  const { state } = useLocation();
  const { email, password } = (state || {}) as PropsType;
  if (!email || !password) {
    return (
      <div className="ConfirmRegistration">
        <h1>Você não pode acessar esta pagina neste momento</h1>
      </div>
    );
  }

  const { signIn } = useAuth();
  const [code, setCode] = useState('');
  const [active, setActive] = useState(false);
  const [snackbar, setSnackbar] = useState<undefined | string>('');
  const [snackbarSeverity, setSnackbarSeverity] = useState<AlertColor>('error');

  useEffect(() => {
    setActive(Boolean(code));
  }, [code]);

  const handleConfirmCode = async () => {
    try {
      await cognitoConfirmCode(email, code);
      const { data } = await signIn({
        email,
        password,
        create: true
      });
      if (data) {
        navigate('/perfil', {
          replace: true
        });
        return;
      } else {
        setSnackbarSeverity('error');
        setSnackbar('Falha ao confirmar o código');
      }
    } catch (err) {
      console.log(err);
      const message = (err as Error).message;
      if (message.includes('Invalid verification code provided, please try again.')) {
        setSnackbarSeverity('error');
        setSnackbar('Código incorreto.');
        return;
      }
      setSnackbarSeverity('error');
      setSnackbar('Falha ao confirmar o código');
    }
  };

  const handleSendCode = async () => {
    try {
      await cognitoSendVerificationCode(email);
      setSnackbarSeverity('success');
      setSnackbar(`Codigo enviado para ${email}`);
    } catch (err) {
      setSnackbarSeverity('error');
      setSnackbar('Falha ao reenviar o código');
    }
  };

  return (
    <div className="ConfirmRegistration">
      <h1>Confirme seu email</h1>
      <OutlinedInput
        id="email_input"
        onChange={(e) => setCode(e.target.value)}
        type="email"
        placeholder={'Código de Verificação'}
        value={code}
      />
      <span className="SpanDefault">Insira o código enviado para {email}</span>
      <div className="SpanInfo">
        <Button className="Button" disabled={!active} onClick={handleConfirmCode}>
          Enviar
        </Button>
        <Button className="Button" onClick={handleSendCode}>
          Reenviar Código
        </Button>
      </div>
      <Snack
        open={Boolean(snackbar)}
        onClose={setSnackbar}
        severity={snackbarSeverity}
        message={snackbar}
      />
    </div>
  );
};
