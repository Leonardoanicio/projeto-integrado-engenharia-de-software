import './styles.css';

import React, { useEffect, useState } from 'react';
import * as Mui from '@mui/material';
import {Add, Remove} from '@mui/icons-material';
import api from '../../services/api';
import { useAuth } from '../../hooks/auth';

type disciplinePacks = {
  id: string;
  packageName: string;
  owner: string;
  details: string;
  value: number;
  lessons: number;
  user: {
    picture: string;
    name: string;
  };
  agenda: Array<any>;
}

const days = [
  'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab', 'Dom'
];

type propsType = {
  setShowPacksForm: (x: boolean) => void;
  setLoading: (x: boolean) => void;
  setDisciplinePacks: (x: disciplinePacks) => void;
  discipline: string;
  email: string;
  selectedPack: disciplinePacks | undefined;
  disciplineName: string;
}

export const PackageForm = ({ setShowPacksForm, setDisciplinePacks, setLoading, discipline, disciplineName, email, selectedPack}: propsType ) => {
  const { user } = useAuth();
  const [name, setName] = useState(selectedPack?.packageName ?? '');
  const [details, setDetails] = useState(selectedPack?.details ?? '');
  const [value, setValue] = useState<number>(selectedPack?.value ?? 0);
  const [lessons, setLessons] = useState<number>(selectedPack?.lessons ?? 0);
  const [agenda, setAgenda] = useState(selectedPack?.agenda ?? []);
  const [enableSubmit, setEnableSubmit] = useState(false);

  const re = new RegExp('^[0-9]*$');
  const intCheck = (newValue, originalValue) => {
    if (re.test(newValue)) {
      if (newValue === '') return newValue;
      const intValue = parseInt(newValue);
      return intValue > 0 ? intValue : originalValue;
    }
    if (!originalValue || originalValue.includes('-')) return 0;
    return originalValue;
  };

  const handleValueChange = (newValue) => {
    setValue(intCheck(newValue, value));
  };

  const handleLessonsChange = (newValue) => {
    setLessons(intCheck(newValue, lessons));
  };

  const handleAgendaChange = (value, key, index) => {
    const newAgenda = [...agenda];
    newAgenda[index][key] = value;
    setAgenda(newAgenda);
  };

  const submit = () => {
    const pack = {
      packageName: name,
      owner: email,
      details,
      discipline,
      disciplineName,
      agenda: agenda,
      value: value,
      lessons: lessons,
      id: selectedPack?.id
    };
    const method = selectedPack ? api.put : api.post;
    method('/package', pack).then(response => {
      const {status, data} = response;
      if (status === 200) {
        const { id } = data;
        setDisciplinePacks({
          ...pack,
          id,
          user: {
            picture: user?.profilePicture || '',
            name: user?.name || ''
          }
        });
      }
      setShowPacksForm(false);
      setLoading(false);
    }).catch((error) => {
      console.error(error);
      setShowPacksForm(false);
      setLoading(false);
    });
  };

  useEffect(() => {
    const isFormValid: boolean = name.length > 0 && 
      details.length > 0 && 
      value > 0 && 
      lessons > 0 && 
      agenda.length > 0 &&
      agenda.every((day) => day.begin && day.end && day.begin < day.end);
    setEnableSubmit(isFormValid);
  }, [agenda, name, details, value, lessons]);

  return (
    <div className="column">
      <h1>Informe os dados do novo pacote</h1>
      <div className='row'>
        <div className="subcolumn">
          <div className="row">
            <span style={{marginLeft: '1%'}}>Nome do pacote: </span>
            <Mui.Input
              id="name_input"
              onChange={(e) => setName(e.target.value)}
              type="text"
              placeholder="Nome"
              sx={{width: '40%'}}
              value={name}/>
          </div>
          <div className="row">
            <span style={{marginLeft: '1%'}}>Valor do pacote: </span>
            <Mui.Input
              id="value_input"
              onChange={(e) => handleValueChange(e.target.value)}
              type="number"
              value={value}
              sx={{width: '40%'}}
            />
          </div>
          <div className="row">
            <span style={{marginLeft: '1%'}}>Quantidade de aulas: </span>
            <Mui.Input
              id="lessons_input"
              onChange={(e) => handleLessonsChange(e.target.value)}
              type="number"
              value={lessons}
              sx={{width: '40%'}}
            />
          </div>
          <span style={{marginLeft: '1%'}}>Forneça uma descrição do pacote: </span>
          <Mui.TextField
            id="details_input"
            multiline
            maxRows={5}
            onChange={(e) => setDetails(e.target.value)}
            type="text"
            placeholder="Detalhes"
            value={details}
            sx={{
              '& .MuiInputBase-root': {
                height: '15rem'
              }
            }}
          />
        </div>
        <div className='subcolumn-center'>
          <span>Quais os dias e horários disponíveis para as aulas?</span>
          <div style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            overflow: 'auto'
          }}>
            <Mui.FormGroup>
              {
                agenda.map((spot, index) => (
                  <div
                    key={index}
                    style={{
                      display: 'flex', 
                      flexDirection: 'row', 
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <Mui.Select variant="standard"
                      value={agenda[index].day ?? days[0]}
                      onChange={(e) => handleAgendaChange(e.target.value, 'day', index)}
                    >
                      {
                        days.map((day, index) => (
                          <Mui.MenuItem 
                            key={index} value={day}>{day}</Mui.MenuItem>
                        ))}
                    </Mui.Select>
                    &nbsp; <span> de </span> &nbsp;&nbsp;
                    <Mui.Input 
                      type='time' 
                      error={Boolean(!agenda[index].begin && agenda[index].end) || 
                        Boolean(agenda[index].begin > agenda[index].end)}
                      value={agenda[index].begin}
                      onChange={(e) => handleAgendaChange(e.target.value, 'begin', index)}
                    /> &nbsp;
                    <span> às </span> &nbsp;&nbsp;
                    <Mui.Input 
                      type='time'
                      error={
                        Boolean(agenda[index].begin && !agenda[index].end) || 
                        Boolean(agenda[index].begin > agenda[index].end)
                      }
                      value={agenda[index].end}
                      onChange={(e) => handleAgendaChange(e.target.value, 'end', index)}
                    />
                    <Mui.IconButton onClick={() => setAgenda(agenda.filter((_, i) => i !== index))}><Remove/></Mui.IconButton>
                  </div>
                ))
              }
              <Mui.IconButton onClick={() => setAgenda([...agenda, {begin: '', end: '', day: days[0]}])}><Add/></Mui.IconButton>
            </Mui.FormGroup>
          </div>
        </div>
      </div>
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', minWidth: '200px'}}>
        <Mui.Button disabled={!enableSubmit} onClick={() => submit()}>Confirmar</Mui.Button>
        <Mui.Button onClick={() => setShowPacksForm(false)}>Voltar</Mui.Button>
      </div>
    </div>
  );
};
