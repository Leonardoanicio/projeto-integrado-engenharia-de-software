import './CashForm.css';

import React, { useState } from 'react';
import {
  Button,
  Select,
  MenuItem,
  OutlinedInput,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup
} from '@mui/material';
import { Pix, CreditCard, Description } from '@mui/icons-material';
import MonetizationOnRoundedIcon from '@mui/icons-material/MonetizationOnRounded';
import { useAuth } from '../../hooks/auth';
import api from '../../services/api';
import { Snack } from '../../components/Alert/Alert';
import { ReactEventType } from '../../types/commonType';
import { AlertColor } from '@mui/material/Alert';

export const CashForm = () => {
  const { user, updateUser } = useAuth();

  const [cardName, setCardName] = useState<string>();
  const [cardNumber, setCardNumber] = useState<string>();
  const [cardCVV, setCardCVV] = useState<string>();
  const [cardExpiration, setCardExpiration] = useState<string>();
  const [cardParcel, setCardParcel] = useState<number>(0);
  const [pingos, setPingos] = useState<number>(0);

  const [snackbar, setSnackbar] = useState<string | undefined>('');
  const [snackbarSeverity, setSnackbarSeverity] = useState<AlertColor>('success');

  const sx_override = {
    '& .MuiSvgIcon-root': {
      fontSize: '2.5vw',
      fontColor: 'var(--main-text-gray)'
    }
  };

  const renderCard = () => (
    <>
      <h1 className="SubHeader">Dados do cartão</h1>
      <OutlinedInput
        id="CardName"
        type="text"
        placeholder="Nome do titular"
        value={cardName}
        onChange={(e) => setCardName(e.target.value)}></OutlinedInput>
      <OutlinedInput

        id="CardNumber"
        type="text"
        placeholder="Numero do cartão"
        value={cardNumber}
        onChange={(e) => setCardNumber(e.target.value)}></OutlinedInput>
      <div className="FlexMulticols" style={{ justifyContent: 'space-between' }}>
        <OutlinedInput
          style={{
            width: '29%'
          }}
          id="cardCVV"
          type="text"
          placeholder="CVV"
          value={cardCVV}
          onChange={(e) => setCardCVV(e.target.value)}></OutlinedInput>
        <OutlinedInput
          style={{
            width: '29%'
          }}
          id="cardExpiration"
          type="text"
          placeholder="Validade"
          value={cardExpiration}
          onChange={(e) => setCardExpiration(e.target.value)}></OutlinedInput>
        <Select
          style={{
            width: '40%'
          }}
          id="cardParcel"
          value={cardParcel}
          onChange={(e) => setCardParcel(parseInt(String(e.target.value)))}>
          {Array.from(Array(10).keys()).map((i) => (
            <MenuItem value={i} key={i}>
              {`${i + 1}${i < 6 ? 'x sem juros' : 'x 1.5% a.m.'}`}
            </MenuItem>
          ))}
        </Select>
      </div>
    </>
  );

  const re = new RegExp('^[0-9]*$');
  const handleChange = ({ target: { value } }: ReactEventType) => {
    if (re.test(value)) {
      const intValue = parseInt(value);
      setPingos(intValue > 0 ? intValue : 0);
      return;
    }
    setPingos(pingos);
  };

  const handleClick = () => {
    if (!user) return;
    const credits = (user?.credits ?? 0) + pingos;
    api
      .patch(`/users/${user.email}`, {
        changes: {
          credits: credits
        }
      })
      .then((res) => {
        if (res.status === 200) {
          updateUser({
            credits: credits
          });
          setSnackbarSeverity('success');
          setSnackbar('Compra feita com sucesso!');
        } else {
          setSnackbarSeverity('error');
          setSnackbar('Falha ao realizar a compra');
        }
      })
      .catch((err) => {
        setSnackbarSeverity('error');
        setSnackbar(err.response.data);
      });
  };

  return (
    <div className="CashForm">
      <div className="FlexRow">
        <h1 className="SubHeader">Quantos pingos você deseja comprar?</h1>
        <OutlinedInput
          style={{
            width: '15%',
            paddingInlineStart: '1vw'
          }}
          placeholder=""
          type="number"
          value={pingos}
          onChange={(e) => handleChange(e)}
          endAdornment={
            <MonetizationOnRoundedIcon
              style={{
                fontSize: '2vw',
                color: 'gold'
              }}
            />
          }></OutlinedInput>
        <div className="FlexMulticols">
          <div className="FlexRow">
            <h1 className="SubHeader">Método de pagamento</h1>
            <FormControl>
              <RadioGroup
                aria-labelledby="demo-radio-buttons-group-label"
                defaultValue="card"
                name="radio-buttons-group">
                <FormControlLabel
                  value="card"
                  control={<Radio />}
                  sx={sx_override}
                  label={
                    <div className="FlexMulticols">
                      <CreditCard style={{ marginRight: '0.25vw' }} />
                      <div
                        className="FlexRow"
                        style={{
                          alignItems: 'flex-start'
                        }}>
                        <span className="PaymentMethod">Cartão de crédito</span>
                        <span className="PaymentDescription">Até 6x sem juros</span>
                      </div>
                    </div>
                  }
                />
                <FormControlLabel
                  disabled={true}
                  value="boleto"
                  control={<Radio />}
                  sx={sx_override}
                  label={
                    <div className="FlexMulticols">
                      <Description style={{ marginRight: '0.25vw' }} />
                      <div
                        className="FlexRow"
                        style={{
                          alignItems: 'flex-start'
                        }}>
                        <span className="PaymentMethod">Boleto</span>
                        <span className="PaymentDescription">Aprovação em até 3 dias</span>
                      </div>
                    </div>
                  }
                />
                <FormControlLabel
                  disabled={true}
                  value="pix"
                  control={<Radio />}
                  sx={sx_override}
                  label={
                    <div className="FlexMulticols">
                      <Pix style={{ marginRight: '0.25vw' }} />
                      <div
                        className="FlexRow"
                        style={{
                          alignItems: 'flex-start'
                        }}>
                        <span className="PaymentMethod">Pix</span>
                        <span className="PaymentDescription">Aprovação imediata</span>
                      </div>
                    </div>
                  }
                />
              </RadioGroup>
            </FormControl>
          </div>
          <div className="FlexRow">
            {renderCard()}
            <Button
              style={{
                marginTop: '0.5vw'
              }}
              disabled={!pingos || pingos <= 0}
              onClick={handleClick}>
              Confirmar
            </Button>
          </div>
        </div>
      </div>
      <Snack
        open={Boolean(snackbar)}
        onClose={setSnackbar}
        severity={snackbarSeverity}
        message={snackbar}
      />
    </div>
  );
};
