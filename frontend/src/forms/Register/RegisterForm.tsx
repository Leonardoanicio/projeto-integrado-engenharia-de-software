import './RegisterForm.css';

import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { EmailInput } from '../../components/Inputs/Email/EmailInput';
import { PasswordInput } from '../../components/Inputs/Password/PasswordInput';
import { RadioButton } from '../../components/Buttons/RadioButton/RadioButton';
import { useNavigate } from 'react-router-dom';
import { Button } from '@mui/material';

import { useAuth } from '../../hooks/auth';
import { Snack } from '../../components/Alert/Alert';

export const RegisterForm = () => {
  const { signUp } = useAuth();
  const navigate = useNavigate();

  const [active, setActive] = useState<boolean>(false);
  const [email, setEmail] = useState<string>('');
  const [emailWarning, setEmailWarning] = useState<string | JSX.Element>('');
  const [password, setPassword] = useState<string>('');
  const [passwordConfirmation, setPasswordConfirmation] = useState<string>('');
  const [passwordWarning, setPasswordWarning] = useState<string | JSX.Element>('');
  const [passwordConfirmationWarning, setPasswordConfirmationWarning] = useState<
    string | JSX.Element
  >('');
  const [userType, setUserType] = useState<string>('aluno');
  const [snackbar, setSnackbar] = useState<string | undefined>('');

  useEffect(() => {
    if ((password !== '' || passwordConfirmation !== '') && password !== passwordConfirmation) {
      setPasswordConfirmationWarning('As senhas não conferem');
    } else {
      setPasswordConfirmationWarning('');
    }

    if (
      userType !== '' &&
      password &&
      email &&
      !passwordWarning &&
      !emailWarning &&
      password === passwordConfirmation
    ) {
      setActive(true);
    } else {
      setActive(false);
    }
  }, [password, passwordConfirmation, email, userType]);

  const handleClick = async () => {
    const response = await signUp({
      email: email,
      userType: userType,
      password: password
    });

    if (response && response.includes('An account with the given email already exists.')) {
      setSnackbar('Email indisponível');
    } else {
      navigate('/confirmar', {
        replace: true,
        state: {
          email: email,
          password: password,
          userType: userType
        }
      });
    }
  };

  return (
    <div className="RegisterForm">
      <h1>Cadastro</h1>
      <EmailInput
        email={email}
        warning={emailWarning}
        setEmail={setEmail}
        setWarning={setEmailWarning}
      />
      <PasswordInput
        placeholder="Senha"
        password={password}
        warning={passwordWarning}
        setPassword={setPassword}
        setWarning={setPasswordWarning}
      />
      <PasswordInput
        placeholder="Confirmar senha"
        password={passwordConfirmation}
        warning={passwordConfirmationWarning}
        setPassword={setPasswordConfirmation}
      />
      <RadioButton
        name="TipoCadastro"
        value={userType}
        onChange={setUserType}
        radios={[
          { value: 'aluno', label: 'Sou Aluno' },
          { value: 'professor', label: 'Sou Professor' }
        ]}
      />
      <Button className="LoginButton" disabled={!active} onClick={handleClick}>
        Cadastrar
      </Button>
      <br />
      <div className="SpanInfo">
        <span className="SpanDefault">Já possiu conta?</span>
        <Link to="/login" className="SpanLink">
          &nbsp;Login
        </Link>
      </div>
      <Snack open={Boolean(snackbar)} onClose={setSnackbar} severity="error" message={snackbar} />
    </div>
  );
};
