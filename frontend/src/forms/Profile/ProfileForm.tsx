import './ProfileForm.css';

import React, { useEffect, useState } from 'react';
import { Button } from '@mui/material';
import { useAuth } from '../../hooks/auth';
import OutlinedInput from '@mui/material/OutlinedInput';
import api from '../../services/api';
import { useCallback } from 'react';
import { Snack } from '../../components/Alert/Alert';
import { ReactEventType } from '../../types/commonType';
import { AlertColor } from '@mui/material/Alert';

export const ProfileForm = () => {
  const { user, updateUser } = useAuth();
  const [name, setName] = useState<string>(user?.name ?? '');
  const [cellphone, setCellphone] = useState<string>(user?.cellphone ?? '');
  const [document, setDocument] = useState<string>(user?.document ?? '');

  const [address, setAddress] = useState<string>(user?.address?.main ?? '');
  const [addressNumber, setAddressNumber] = useState<string>(user?.address?.number ?? '');
  const [addressComplement, setAddressComplement] = useState<string>(
    user?.address?.complement ?? ''
  );
  const [addressCEP, setAddressCEP] = useState<string>(user?.address?.CEP ?? '');

  const [buttonEnable, setButtonEnable] = useState<boolean>(false);
  const [snackbar, setSnackbar] = useState<string | undefined>('');
  const [snackbarSeverity, setSnackbarSeverity] = useState<AlertColor>('success');

  const handleCellphone = ({ target: { value } }: ReactEventType) => {
    const cellPhoneDigits = Array.from(value).filter((element) => /[0-9]/.test(element));

    if (cellPhoneDigits.length > 10) {
      cellPhoneDigits.splice(0, 0, '(');
      cellPhoneDigits.splice(3, 0, ') ');
      cellPhoneDigits.splice(9, 0, '-');
    } else if (cellPhoneDigits.length > 6) {
      cellPhoneDigits.splice(0, 0, '(');
      cellPhoneDigits.splice(3, 0, ') ');
      cellPhoneDigits.splice(8, 0, '-');
    } else if (cellPhoneDigits.length > 2) {
      cellPhoneDigits.splice(0, 0, '(');
      cellPhoneDigits.splice(3, 0, ') ');
    } else if (cellPhoneDigits.length > 0) cellPhoneDigits.splice(0, 0, '(');

    const cellphone = cellPhoneDigits.reduce((acc, cur) => `${acc}${cur}`, '');
    setCellphone(cellphone);
  };

  const handleCEP = ({ target: { value } }: ReactEventType) => {
    const CEPArray = Array.from(value).filter((element) => /[0-9]/.test(element));
    if (CEPArray.length > 5) {
      CEPArray.splice(5, 0, '-');
    }
    const CEP = CEPArray.reduce((acc, cur) => `${acc}${cur}`, '');
    setAddressCEP(CEP);
  };

  const handleDocument = ({ target: { value } }: ReactEventType) => {
    const documentNumbers = Array.from(value).filter((element) => /[0-9]/.test(element));
    if (documentNumbers.length > 11) {
      documentNumbers.splice(2, 0, '.');
      documentNumbers.splice(6, 0, '.');
      documentNumbers.splice(10, 0, '/');
      if (documentNumbers.length > 15) {
        documentNumbers.splice(15, 0, '-');
      }
    } else if (documentNumbers.length > 9) {
      documentNumbers.splice(3, 0, '.');
      documentNumbers.splice(7, 0, '.');
      if (documentNumbers.length > 11) {
        documentNumbers.splice(11, 0, '-');
      }
    } else if (documentNumbers.length > 6) {
      documentNumbers.splice(3, 0, '.');
      if (documentNumbers.length > 7) {
        documentNumbers.splice(7, 0, '.');
      }
    } else if (documentNumbers.length > 3) {
      documentNumbers.splice(3, 0, '.');
    }
    const document = documentNumbers.reduce((acc, cur) => `${acc}${cur}`, '');
    setDocument(document);
  };

  const isValidCEP = useCallback((addressCEP) => {
    return addressCEP.length === 0 || addressCEP.length === 9;
  }, []);

  const isValidCellPhone = useCallback((cellphone) => {
    return cellphone.length >= 14 || cellphone.length === 0;
  }, []);

  const isValidDocument = useCallback((document) => {
    return document.length === 14 || document.length === 18 || document.length === 0;
  }, []);

  const submit = async () => {
    if (!user?.email) return;

    const payload = {
      address: {
        CEP: addressCEP || undefined,
        number: addressNumber || undefined,
        main: address || undefined,
        complement: addressComplement || undefined
      },
      name: name || undefined,
      cellphone: cellphone || undefined,
      document: document || undefined
    };

    api
      .patch(`/users/${user.email}`, {
        changes: payload
      })
      .then(() => {
        setSnackbarSeverity('success');
        setSnackbar('Dados atualizados!');
        updateUser(payload);
      })
      .catch(() => {
        setSnackbarSeverity('error');
        setSnackbar('Erro ao atualizar os dados!');
      });
  };

  useEffect(() => {
    const validation =
      isValidCEP(addressCEP) && isValidCellPhone(cellphone) && isValidDocument(document);

    setButtonEnable(validation);
  }, [document, addressCEP, cellphone, isValidCEP, isValidCellPhone, isValidDocument]);

  useEffect(() => {
    setName(user?.name ?? '');
    setCellphone(user?.cellphone ?? '');
    setDocument(user?.document ?? '');
    setAddress(user?.address?.main ?? '');
    setAddressNumber(user?.address?.number ?? '');
    setAddressCEP(user?.address?.CEP ?? '');
    setAddressComplement(user?.address?.complement ?? '');
  }, [user]);

  return (
    <div className="ProfileForm">
      <div className="FlexRow">
        <h1>Dados da conta</h1>
        <OutlinedInput
          id="name_input"
          onChange={(e) => setName(e.target.value)}
          type="text"
          placeholder="Nome"
          value={name}
        />
        <div className="FlexMulticols">
          <div
            className="FlexSubColumn W M"
            style={{
              ['--width' as string]: '49.5%',
              ['--margin' as string]: '0.2em 0.2em 0.2em 0'
            }}>
            <OutlinedInput
              id="addr_input"
              onChange={handleCellphone}
              type="text"
              placeholder="Telefone"
              value={cellphone}
              inputProps={{
                maxLength: 15
              }}
            />
          </div>
          <div
            className="FlexSubColumn W M"
            style={{
              ['--width' as string]: '49.5%',
              ['--margin' as string]: '0.2em 0.01em 0.2em 0.2em'
            }}>
            <OutlinedInput
              id="doc_input"
              onChange={handleDocument}
              type="text"
              placeholder="CPF/CNPJ"
              value={document}
              inputProps={{
                maxLength: 18
              }}
            />
          </div>
        </div>
        <div className="FlexSubColumn W M" style={{
          ['--width' as string]: '100%',
          ['--margin' as string]: '0.2em 0.2em 0.2em 0',
          height: 'auto'
        }}>
          <OutlinedInput
            id="addr_input"
            onChange={(e) => setAddress(e.target.value)}
            type="text"
            placeholder="Endereço"
            value={address}
          />
        </div>
        <div className="FlexMulticols">
          <div
            className="FlexSubColumn W M"
            style={{
              ['--width' as string]: '32.3%',
              ['--margin' as string]: '0.2em 0.2em 0.2em 0'
            }}>
            <OutlinedInput
              id="addr_input"
              onChange={(e) => setAddressNumber(e.target.value)}
              type="text"
              placeholder="Número"
              value={addressNumber}
            />
          </div>
          <div
            className="FlexSubColumn W M"
            style={{
              ['--width' as string]: '32.3%',
              ['--margin' as string]: '0.2em'
            }}>
            <OutlinedInput
              id="addrNumber_input"
              onChange={(e) => setAddressComplement(e.target.value)}
              type="text"
              placeholder="Complemento"
              value={addressComplement}
            />
          </div>
          <div
            className="FlexSubColumn W"
            style={{
              ['--width' as string]: '32.3%',
              ['--margin' as string]: '0.2em 0 0.2em 0.2em'
            }}>
            <OutlinedInput
              id="CEP_input"
              onChange={handleCEP}
              type="text"
              placeholder="CEP"
              value={addressCEP}
              inputProps={{
                maxLength: 9
              }}
            />
          </div>
        </div>
        <Button
          className="SaveButton W"
          style={{
            ['--width' as string]: '40%'
          }}
          disabled={!buttonEnable}
          onClick={submit}>
          {' '}
          Salvar{' '}
        </Button>
      </div>
      <Snack
        open={Boolean(snackbar)}
        onClose={setSnackbar}
        severity={snackbarSeverity}
        message={snackbar}
      />
    </div>
  );
};
