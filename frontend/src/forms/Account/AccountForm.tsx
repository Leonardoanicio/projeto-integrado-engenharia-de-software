import './AccountForm.css';

import React, { useEffect, useState } from 'react';
import { Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';

import { PasswordInput } from '../../components/Inputs/Password/PasswordInput';
import { Snack } from '../../components/Alert/Alert';
import { ConfirmationDialog } from '../../components/Dialog/ConfirmationDialog';

import { useAuth } from '../../hooks/auth';
import api from '../../services/api';

import { AlertColor } from '@mui/material/Alert';

export const AccountForm = () => {
  const { user, accessToken, updateUser, signOut } = useAuth();
  const navigator = useNavigate();
  const [accountStatus, setAccountStatus] = useState<boolean>(true);

  const [password, setPassword] = useState<string>('');
  const [passwordWarning, setPasswordWarning] = useState<string | JSX.Element>('');
  const [newPassword, setNewPassword] = useState<string>('');
  const [newPasswordConfirmation, setNewPasswordConfirmation] = useState<string>('');
  const [newPasswordWarning, setNewPasswordWarning] = useState<string | JSX.Element>('');
  const [newPasswordConfirmationWarning, setNewPasswordConfirmationWarning] = useState<
    string | JSX.Element
  >('');
  const [passwordButton, setPasswordButton] = useState<boolean>(false);

  const [snackbar, setSnackbar] = useState<string | undefined>('');
  const [snackbarSeverity, setSnackbarSeverity] = useState<AlertColor>('success');

  const [hideAccount, setHideAccount] = useState<boolean>(false);
  const [deleteAccount, setDeleteAccount] = useState<boolean>(false);

  useEffect(() => {
    if (!user) {
      navigator('/', {
        replace: true
      });
      return;
    }
    setAccountStatus(!!user?.active);
  }, [user]);

  useEffect(() => {
    setPasswordWarning('');
    if (
      (newPassword !== '' || newPasswordConfirmation !== '') &&
      newPassword !== newPasswordConfirmation
    ) {
      setNewPasswordConfirmationWarning('As senhas não conferem');
      setPasswordButton(false);
    } else {
      setNewPasswordConfirmationWarning('');
      setPasswordButton(newPassword !== '' && password !== '' && newPasswordWarning === '');
    }
  }, [password, newPassword, newPasswordConfirmation, newPasswordWarning]);

  const clearForm = () => {
    setPassword('');
    setNewPassword('');
    setNewPasswordConfirmation('');
    setPasswordWarning('');
    setNewPasswordWarning('');
    setNewPasswordConfirmationWarning('');
  };

  const handlePasswordChange = async () => {
    if (!user?.email) {
      setSnackbarSeverity('error');
      setSnackbar('Falha ao atualizar os dados');
      return;
    }
    api
      .patch('/password', {
        accessToken: accessToken,
        email: user.email,
        oldPassword: password,
        newPassword: newPassword
      })
      .then((res) => {
        if (res.status === 200) {
          clearForm();
          setSnackbarSeverity('success');
          setSnackbar('Dados atualizados!');
        } else {
          setSnackbarSeverity('error');
          setSnackbar('Falha ao atualizar os dados');
        }
      })
      .catch((err) => {
        setSnackbarSeverity('error');
        setSnackbar(err.response.data);
      });
  };

  const handleAccountHide = async () => {
    if (!user?.email) {
      setSnackbarSeverity('error');
      setSnackbar('Falha ao atualizar os dados');
      return;
    }
    api
      .patch(`/users/${user.email}`, {
        changes: {
          active: !user?.active
        }
      })
      .then((res) => {
        if (res.status === 200) {
          clearForm();
          updateUser({
            active: !user?.active
          });
          setSnackbarSeverity('success');
          setSnackbar('Dados atualizados!');
        } else {
          setSnackbarSeverity('error');
          setSnackbar('Falha ao atualizar os dados');
        }
      })
      .catch((err) => {
        setSnackbarSeverity('error');
        setSnackbar(err.response.data);
      });
    setHideAccount(false);
  };

  const handleAccountDelete = async () => {
    if (!user?.email) {
      setSnackbarSeverity('error');
      setSnackbar('Falha ao atualizar os dados');
      return;
    }
    api
      .patch(`/users/${user.email}`, {
        changes: {
          accountDeleted: true
        }
      })
      .then((res) => {
        if (res.status === 200) {
          clearForm();
          signOut();
          navigator('/', {
            replace: true
          });
          setSnackbarSeverity('success');
          setSnackbar('Dados atualizados!');
        } else {
          setSnackbarSeverity('error');
          setSnackbar('Falha ao atualizar os dados!');
        }
      })
      .catch((err) => {
        setSnackbarSeverity('error');
        setSnackbar(err.response.data);
      });
    setDeleteAccount(false);
  };

  return (
    <div className="AccountForm">
      <div className="FlexRow" style={{
        width: '50%',
      }}>
        <h1 className="SubHeader">Alteração de senha</h1>
        <PasswordInput
          placeholder="Senha atual"
          password={password}
          warning={passwordWarning}
          setPassword={setPassword}
        />
        <PasswordInput
          placeholder="Nova senha"
          password={newPassword}
          setPassword={setNewPassword}
          warning={newPasswordWarning}
          setWarning={setNewPasswordWarning}
          sx={{ margin: '0.25rem' }}
        />
        <PasswordInput
          placeholder="Confirme a nova senha"
          password={newPasswordConfirmation}
          warning={newPasswordConfirmationWarning}
          setPassword={setNewPasswordConfirmation}
        />
        <Button
          className="SaveButton"
          sx={{
            width: '20%',
            margin: '0.4em 0 0 0'
          }}
          onClick={handlePasswordChange}
          disabled={!passwordButton}>
          {' '}
          Salvar{' '}
        </Button>
        <br />
        <br />
        <h1 className="SubHeader BM">Status da conta</h1>
        <div className="FlexMulticols">
          <div className="FlexSubColumn W" style={{ ['--width' as string]: '30%', margin: '0'}}>
            <Button
              className={accountStatus ? 'HideAccount' : 'UnHideAccount'}
              onClick={() => setHideAccount(true)}>
              {' '}
              {accountStatus ? 'Conta Visível' : 'Conta Invisível'}{' '}
            </Button>
          </div>
          <div className="FlexSubColumn W" style={{ ['--width' as string]: '30%', margin: '0' }}>
            <Button className="UnHideAccount" onClick={() => setDeleteAccount(true)}>
              {' '}
              Excluir Conta{' '}
            </Button>
          </div>
        </div>
      </div>
      <ConfirmationDialog
        open={hideAccount}
        handleClose={() => setHideAccount(false)}
        handleConfirm={handleAccountHide}
        title="Deseja esconder a sua conta?"
        content="
                    Essa ação pode ser revertida a qualquer momento.
                    Seus dados e transações serão mantidos, porém você não irá mais aparecer na lista de 
                    usuários. 
                "
      />
      <ConfirmationDialog
        open={deleteAccount}
        handleClose={() => setDeleteAccount(false)}
        handleConfirm={handleAccountDelete}
        title="Deseja excluir a sua conta?"
        content="
                    Essa ação é ireversível.
                    Você não terá mais acesso a nenhum dos seus dados e transações.
                    Você não poderá criar uma outra conta utilizando o email atual. 
                    Se você deseja apenas obter uma ausência temporária, opte por esconder sua conta.
                "
      />
      <Snack
        open={Boolean(snackbar)}
        onClose={setSnackbar}
        severity={snackbarSeverity}
        message={snackbar}
      />
    </div>
  );
};
