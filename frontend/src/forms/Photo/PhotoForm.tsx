import './PhotoForm.css';

import axios from 'axios';
import React, { useState } from 'react';
import { Box, OutlinedInput, Button, CircularProgress } from '@mui/material';
import { useAuth } from '../../hooks/auth';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import api from '../../services/api';
import { Snack } from '../../components/Alert/Alert';
import { ReactEventType } from '../../types/commonType';

const validFileExtensions = ['png', 'jpg', 'jpeg'];

export const PhotoForm = () => {
  const { user, updateUser } = useAuth();
  const [imagePath, setImagePath] = useState<string | undefined>(undefined);
  const [binaryImage, setBinaryImage] = useState<Blob | undefined>(undefined);
  const [uploadedImage, setUploadedImage] = useState<string | undefined>(undefined);
  const [loading, setLoading] = useState<boolean>(false);

  const [snackbar, setSnackbar] = useState<string | undefined>('');

  const uploadUserPhoto = async () => {
    if (!binaryImage || !imagePath || !user?.email) {
      return;
    }
    setLoading(true);

    const urlSearchParams = [
      ['email', user.email],
      ['filename', imagePath]
    ];
    const params = new URLSearchParams(urlSearchParams);

    const preSignedUrl = await api.get('/generate_url', { params });

    const response = await axios({
      method: 'put',
      url: preSignedUrl.data,
      data: binaryImage,
      headers: {
        'Content-Type': ''
      }
    });

    if (response.status === 200) {
      updateUser({ profilePicture: uploadedImage });
    }
    setLoading(false);
  };

  const handleFileUpload = (e: ReactEventType) => {
    const {
      target: { files }
    } = e as React.ChangeEvent<HTMLInputElement>;
    if (!files) {
      return;
    }
    const file = files[0];
    const fileExtension = file.name.split('.').slice(-1)[0];
    if (!validFileExtensions.includes(fileExtension)) {
      setSnackbar('Formato inválido. Use .png/.jpg/.jpeg');
      return;
    }
    if (file.size > 500*1024) {
      setSnackbar('Tamanho máximo de 500kb');
      return;
    }
    setLoading(true);
    setImagePath(file.name);

    const imageReader = new FileReader();
    imageReader.readAsDataURL(file);

    imageReader.onload = async () => {
      const imageReaderResult = imageReader.result as string;
      setUploadedImage(imageReaderResult);

      const res = await fetch(imageReaderResult);
      const imageBlob = await res.blob();
      setBinaryImage(imageBlob);
    };
    setLoading(false);
  };

  return (
    <div className="PhotoForm">
      <div className="FlexRows">
        {loading ? (
          <CircularProgress />
        ) : (
          <>
            <Box className="OutlinedBox">
              {uploadedImage ? (
                <img src={uploadedImage} className="ProfilePicture" />
              ) : user?.profilePicture ? (
                <img src={user.profilePicture} className="ProfilePicture" />
              ) : (
                <AccountCircleOutlinedIcon className="EmptyProfilePicture" />
              )}
            </Box>
            <br />
            <OutlinedInput
              className="BM"
              style={{
                ['--bmargin' as string]: '1vw'
              }}
              type="file"
              sx={{
                padding: '0.35vw'
              }}
              inputProps={{ accept: 'image/jpeg, image/jpg, image/png' }}
              onChange={(e) => handleFileUpload(e)}></OutlinedInput>
            <Button
              className="SaveButton W"
              style={{
                ['--width' as string]: '40%'
              }}
              disabled={!binaryImage}
              onClick={uploadUserPhoto}>
              Salvar
            </Button>
          </>
        )}
      </div>
      <Snack severity="error" open={Boolean(snackbar)} onClose={setSnackbar} message={snackbar} />
    </div>
  );
};
