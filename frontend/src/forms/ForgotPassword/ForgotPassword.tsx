import './ForgotPassword.css';

import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { EmailInput } from '../../components/Inputs/Email/EmailInput';
import { RecoverPasswordButton } from '../../components/Buttons/RecoverPassword/RecoverPasswordButton';

export const ForgotPasswordForm = () => {
  const [email, setEmail] = useState<string>('');
  const [active, setActive] = useState<boolean>(false);

  const [emailWarning, setEmailWarning] = useState<string | JSX.Element>('');

  useEffect(() => {
    setActive(!!(email && emailWarning === ''));
  }, [emailWarning, email]);

  return (
    <div className="ForgotPassword">
      <h1>Login</h1>
      <EmailInput
        email={email}
        warning={emailWarning}
        setEmail={setEmail}
        setWarning={setEmailWarning}
      />
      <RecoverPasswordButton active={active} email={email} />
      <br />
      <div className="SpanInfo">
        <span className="SpanDefault">Não possui conta? </span>
        <Link to="/cadastro" className="SpanLink">
          &nbsp;Cadastre-se
        </Link>
      </div>
      <div className="SpanInfo">
        <span className="SpanDefault">Já possiu conta?</span>
        <Link to="/login" className="SpanLink">
          &nbsp;Login
        </Link>
      </div>
    </div>
  );
};
