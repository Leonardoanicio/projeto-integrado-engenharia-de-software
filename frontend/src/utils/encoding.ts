export const hashPassword = async (pwd: string) => {
  const encoder = new TextEncoder();
  const pwdEncode = encoder.encode(pwd);
  const pwdDigested = await crypto.subtle.digest('SHA-256', pwdEncode);
  const pwdArray = Array.from(new Uint8Array(pwdDigested));
  return pwdArray.map((b) => b.toString(16).padStart(2, '0')).join('');
};
