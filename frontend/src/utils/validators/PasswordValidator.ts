export const PasswordValidation = (password: string) => {
  const testResults = [];

  const testAtLeastOneNumber = /[0-9]/;
  const testAtLeastOneUpperCase = /[A-Z]/;
  const testAtLeastOneLowerCase = /[a-z]/;
  const testAtLeastOneSpecialCharacter = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>?]/;
  const testAtLeastEightCharacters = /.{8,}/;

  if (!testAtLeastOneNumber.test(password)) {
    testResults.push('Ao menos 1 número');
  }
  if (!testAtLeastOneUpperCase.test(password)) {
    testResults.push('Ao menos 1 letra maiúscula');
  }
  if (!testAtLeastOneLowerCase.test(password)) {
    testResults.push('Ao menos 1 letra minúscula');
  }
  if (!testAtLeastOneSpecialCharacter.test(password)) {
    testResults.push('Ao menos 1 caractere especial');
  }
  if (!testAtLeastEightCharacters.test(password)) {
    testResults.push('Ao menos 8 caracteres');
  }

  return testResults;
};
