export class UserNotFound extends Error {
  constructor() {
    super('Usuário o usenha incorretos');
    Error.captureStackTrace(this, this.constructor);
  }
}
