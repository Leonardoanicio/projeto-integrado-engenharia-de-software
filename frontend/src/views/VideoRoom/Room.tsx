import './styles.css';

import { Participant } from './Participant';
import { useRoom } from 'use-twilio-video';
import React, { useEffect } from 'react';
import api from '../../services/api';
import { useAuth } from '../../hooks/auth';
import {
  useParams,
  useNavigate
} from 'react-router-dom';
import { CircularProgress } from '@mui/material';


import twilioVideo from 'twilio-video';
import { Button } from '@mui/material';

export const Room = () => {
  const [token, setToken] = React.useState('');
  const { roomName }= useParams();
  const navigator = useNavigate();

  const { user } = useAuth();
  const { room, connectRoom, disconnectRoom, localParticipant, remoteParticipants } = useRoom();
    
  useEffect(() => {
    if (user?.email && roomName) {
      api.post('/room', {
        roomName,
        user: user?.email
      })
        .then(({ data }) => {
          const { accessToken } = data;

          twilioVideo.connect(token, { name: roomName, dominantSpeaker: true }).catch(console.error);

          setToken(accessToken);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [user, roomName]);
  
  useEffect(() => {
    if (!room && token && roomName) {
      console.log('Connecting');
      connectRoom({ token, options: { name: roomName, dominantSpeaker: true } });
      return () => disconnectRoom();
    }
  }, [connectRoom, disconnectRoom, room, roomName, token]);

  const handleDisconnect = () => {
    disconnectRoom();
    navigator('/agenda', {
      replace: true
    });
  };

  if (room)
    return (
      <div className='room'>
        <div className='videos'>
          <Participant participant={localParticipant} />
          <div style={{minWidth: '50px'}} />
          {remoteParticipants.map((p, i) => (
            <Participant participant={p} key={i}/>
          ))}
        </div>
        <Button onClick={() => handleDisconnect()} style={{marginBottom: '2%', marginTop: '2%'}}>Desconectar</Button>
      </div>
    );
  
  return <div className='room'><CircularProgress /></div>;
};