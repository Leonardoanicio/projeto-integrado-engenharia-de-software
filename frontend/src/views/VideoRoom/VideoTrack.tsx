import React, { useEffect, useRef } from 'react';

export const VideoTrack = ({ track }: { track: any }) => {
  const ref = useRef();

  useEffect(() => {
    if (track) {
      const el = ref.current;
      track.attach(el);

      return () => {
        track.detach(el);
      };
    }
  }, [track]);

  return <video style={{ width: '700px' }} ref={ref} />;
};