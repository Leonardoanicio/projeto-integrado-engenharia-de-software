import React from 'react';

import { AudioTrack } from './AudioTrack';
import { VideoTrack } from './VideoTrack';
import { useTrack } from 'use-twilio-video';

export const Participant = ({ participant }: { participant: string }) => {
  const { videoOn, audioOn, videoTrack, audioTrack } = useTrack({ participant });

  return (
    <div>
      <VideoTrack track={videoTrack} />
      <br />
      <AudioTrack track={audioTrack} />
    </div>
  );
};

