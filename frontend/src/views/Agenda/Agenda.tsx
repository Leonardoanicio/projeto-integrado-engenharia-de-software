import './Agenda.css';

import React from 'react';
import { styled } from '@mui/material/styles';
import { CircularProgress, Table, TableContainer, TableFooter, TablePagination } from '@mui/material';
import Paper from '@mui/material/Paper';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useAuth } from '../../hooks/auth';
import dayjs from 'dayjs';
import api from '../../services/api';
import { Link } from 'react-router-dom';


export const AgendaView = () => {
  const { user } = useAuth();
  const [agenda, setAgenda] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number,
  ) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  
  const getUserAgenda = () => {
    setLoading(true);
    api.get('/agenda', {
      params: {
        email: user?.email
      }
    }).then((response) => {
      const { data } = response;
      setAgenda(data);
      setLoading(false);
    });
  };

  React.useEffect(() => {
    if (user?.email) {
      getUserAgenda();
    }
  }, [user]);

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.white,
      color: theme.palette.common.black,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const spotsTable = (rows: any) => {
    return (
      <TableContainer component={Paper} className="box" >
        <Table sx={{ minWidth: 900 }} aria-label="customized table" stickyHeader>
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">Pacote</StyledTableCell>
              <StyledTableCell align="center">{user?.userType === 'professor' ? 'Aluno' : 'Professor'} </StyledTableCell>
              <StyledTableCell align="center">Começa às</StyledTableCell>
              <StyledTableCell align="center">Termina às</StyledTableCell>
              <StyledTableCell align="center">Link</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .sort((a,b) => new Date(a.beginsAt).getTime() - new Date(b.beginsAt).getTime())
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => (
                <StyledTableRow key={row.id}>
                  <StyledTableCell align="center">{row.details}</StyledTableCell>
                  <StyledTableCell align="center">{user?.userType === 'professor' ? row.aluno : row.professor}</StyledTableCell>
                  <StyledTableCell align="center">{dayjs(row.beginsAt).format('DD/MM/YY - HH:mm')}</StyledTableCell>
                  <StyledTableCell align="center">{dayjs(row.endsAt).format('DD/MM/YY - HH:mm')}</StyledTableCell>
                  <StyledTableCell align="center">{
                    dayjs(row.beginsAt) < dayjs() && dayjs(row.endsAt) > dayjs() ? 
                      <Link to={`/room/${row.id}`}>Acessar Sala</Link> : 
                      'Indisponível'
                  }</StyledTableCell>
                </StyledTableRow>
              ))}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[10]}
                colSpan={5}
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: {
                    'aria-label': 'rows per page',
                  },
                  native: true,
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    );
  };


  const getCurrentView = () => {
    if (loading) {
      return <CircularProgress />;
    }

    return spotsTable(agenda);
  };

  return (
    <div className="Agenda">
      {getCurrentView()}
    </div>
  );
};
