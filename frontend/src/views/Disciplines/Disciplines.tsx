import './Disciplines.css';

import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, CircularProgress, List, MenuItem, Select } from '@mui/material';
import { useAuth } from '../../hooks/auth';
import api from '../../services/api';
import { DisciplineCard } from '../../components/DisciplineCard';
import { Snack } from '../../components/Alert/Alert';

const disciplines = {
  Inglês: 'english',
  Português: 'portuguese',
  Matemática: 'math',
  Física: 'physics',
  Música: 'music'
};

const reverseDisciplines = Object.keys(disciplines)
  .reduce((acc, cur) =>  {
    return {
      ...acc, 
      [disciplines[cur]]: cur
    };
  }, {});

type userDisciplines = {
  id: string;
  name: string;
  email: string;
}

export const DisciplinesView = () => {
  const { user } = useAuth();
  const navigate = useNavigate();
  const [showDisciplinesForm, setShowDisciplinesForm] = useState(false);
  const [userDisciplines, setUserDiciplines] = useState<userDisciplines[]>([]);
  const [loading, setLoading] = useState(true);
  const [selectedDiscipline, setSelectedDiscipline] = useState(Object.values(disciplines)[0]);
  const [snackbar, setSnackbar] = useState<string | undefined>('');

  const getUserDisciplines = () => {
    if (!user || !user.email) {
      return;
    }
    const path = user.userType === 'professor' ? '/discipline' : 'class';
    api
      .get(path, {
        params: { email: user?.email }
      })
      .then((response) => {
        const { data } = response;
        setUserDiciplines(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
      });
  };
  const putUserDiscipline = () => {
    if(!user || !user.email) return;
    setLoading(true);
    const { email } = user;
    const discipline = {
      email,
      name: selectedDiscipline,
    };
    const path = user.userType === 'professor' ? '/discipline' : 'class';
    api.post(path, {...discipline})
      .then((response) => {
        const {status, data} = response;
        if (status === 200) {
          const id = data;
          setUserDiciplines([
            ...userDisciplines,
            {
              ...discipline,
              id
            }
          ]);
        }
        setShowDisciplinesForm(false);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setShowDisciplinesForm(false);
        setLoading(false);
      });
  };
  const deleteUserDiscipline = (id) => {
    if(!user || !user.email) return;
    setLoading(true);
    const { email } = user;
    const discipline = {
      email,
      id,
    };
    const path = user.userType === 'professor' ? '/discipline' : 'class';
    api.delete(path, { params: discipline })
      .then((response) => {
        const {status, data} = response;
        if (status === 200) {
          const id = data;
          setUserDiciplines(userDisciplines.filter(x => x.id !== id));
        }
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        const { status } = error.response;
        if (status === 400) {
          setSnackbar('Você não pode deletar uma disciplina que possui aulas');
        }
        setLoading(false);
      });
  };
  useEffect(() => getUserDisciplines(), [user]);

  const emptyUserDisciplines = () => (
    <>
      <h2>Vishe! Isso aqui está vazio! Adicione uma nova disciplina clicando em Adicionar.</h2>
      <Button onClick={() => setShowDisciplinesForm(true)}>Adicionar</Button>
    </>
  );

  const emptyUserClasses = () => (
    <>
      <h2>Vishe! Isso aqui está vazio! Busque por novas classes clicando em Buscar.</h2>
      <Button onClick={() => setShowDisciplinesForm(true)}>Buscar</Button>
    </>
  );
    
  const findDisciplines = () => {
    return (
      <>
        <h2>
          Selecione abaixo a disciplina que deseja aprender e clique em <b>Buscar</b>.
        </h2>
        <Select
          className="CustomSelect"
          value={selectedDiscipline}
          onChange={(e) => setSelectedDiscipline(e.target.value)}
        >
          {Object.keys(disciplines).map((item, index) => {
            return (
              <MenuItem className="CustomItem" key={index} value={disciplines[item]}>
                {item}
              </MenuItem>
            );
          })}
        </Select>
        <div>
          <Button onClick={() => {
            navigate('/pacotes', {
              replace: true,
              state: { 
                disciplineName: selectedDiscipline,
                disciplinePrettyName: reverseDisciplines[selectedDiscipline] || selectedDiscipline
              }
            });
          }}>Buscar</Button>
          <Button onClick={() => setShowDisciplinesForm(false)}>Voltar</Button>
        </div>
      </>
    );
  };

  const addUserDisciplines = () => {
    return (
      <>
        <h2>
          Selecione abaixo a disciplina que deseja lecionar e clique em <b>Confirmar</b>.
        </h2>
        <Select
          className="CustomSelect"
          value={selectedDiscipline}
          onChange={(e) => setSelectedDiscipline(e.target.value)}
        >
          {Object.keys(disciplines).map((item, index) => {
            const usedDisciplines = userDisciplines.map(x => x.name);
            if (!usedDisciplines.includes(disciplines[item]))
              return (
                <MenuItem className="CustomItem" key={index} value={disciplines[item]}>
                  {item}
                </MenuItem>
              );
          })}
        </Select>
        <div>
          <Button onClick={() => putUserDiscipline()}>Confirmar</Button>
          <Button onClick={() => setShowDisciplinesForm(false)}>Voltar</Button>
        </div>
      </>
    );
  };

  const listUserDisciplines = () => (
    <>
      <List style={{minWidth: '600px', maxHeight: '500px', overflow: 'auto', display: 'flex', flexWrap: 'wrap', flexDirection: 'row'}}>
        {
          userDisciplines.map(
            discipline => (
              <DisciplineCard 
                key={discipline.id} 
                basename={discipline.name}
                name={reverseDisciplines[discipline.name] || discipline.name} 
                id={discipline.id}
                onRemove={deleteUserDiscipline}
                onVisualize={() => {
                  navigate('/pacotes', {
                    replace: true,
                    state: { 
                      disciplineId: discipline.id, 
                      disciplineName: discipline.name,
                      disciplinePrettyName: reverseDisciplines[discipline.name] || discipline.name 
                    }
                  });
                }}
              />
            )
          )
        }
      </List>
      <Button onClick={() => setShowDisciplinesForm(true)}>Adicionar</Button>
    </>
  );

  const listDisciplinesAluno = () => (
    <>
      <List style={{maxHeight: '500px', overflow: 'auto', display: 'flex', flexWrap: 'wrap'}}>
        {
          userDisciplines.map(
            discipline => (
              <DisciplineCard 
                key={discipline.id} 
                basename={discipline.name}
                name={reverseDisciplines[discipline.name] || discipline.name} 
                id={discipline.id}
                onRemove={deleteUserDiscipline}
                onVisualize={() => {
                  navigate('/agenda', {
                    replace: true,
                  });
                }}
              />
            )
          )
        }
      </List>
      <Button onClick={() => setShowDisciplinesForm(true)}>Adicionar</Button>
    </>
  );

  const getCurrentView = () => {
    if (user?.userType === 'professor') {
      if (loading) {
        return <CircularProgress />;
      }
      if (userDisciplines.length && !showDisciplinesForm) {
        return listUserDisciplines();
      }
      if (showDisciplinesForm) {
        return addUserDisciplines();
      }
      return emptyUserDisciplines();
    } else {
      if (loading) {
        return <CircularProgress />;
      }
      if (userDisciplines.length && !showDisciplinesForm) {
        return listDisciplinesAluno();
      }
      if (showDisciplinesForm) {
        return findDisciplines();
      }
      return emptyUserClasses();
    }
  };

  return (
    <div className="Disciplines">
      { getCurrentView() }
      <Snack open={Boolean(snackbar)} onClose={setSnackbar} severity="error" message={snackbar} />
    </div>
  );
};
