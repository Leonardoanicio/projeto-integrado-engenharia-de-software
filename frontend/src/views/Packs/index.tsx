import './styles.css';

import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Button, CircularProgress, List } from '@mui/material';
import { useAuth } from '../../hooks/auth';
import api from '../../services/api';
import { PackCard } from '../../components/PackCard';
import { Snack } from '../../components/Alert/Alert';
import { PackageForm } from '../../forms/Pack';

type disciplinePacks = {
  id: string;
  packageName: string;
  owner: string;
  details: string;
  value: number;
  lessons: number;
  user: {
    picture: string;
    name: string;
  };
  agenda: Array<any>;
}

type packProps = {
  disciplineId: string;
  disciplineName: string;
  disciplinePrettyName: string;
}

export const PacksView = () => {
  const { user, updateUser } = useAuth();
  const { state } = useLocation();
  const { disciplineId, disciplineName, disciplinePrettyName } = (state || {}) as packProps;
  const navigate = useNavigate();

  const [showPacksForm, setShowPacksForm] = useState(false);
  const [disciplinePacks, setDisciplinePacks] = useState<disciplinePacks[]>([]);
  const [selectedPack, setSelectedPack] = useState<disciplinePacks | undefined>(undefined);
  const [loading, setLoading] = useState(true);
  const [snackbar, setSnackbar] = useState<string | undefined>('');

  const getPackage = () => {
    if (!user || !user.email) {
      return;
    }
    const params = user.userType === 'professor' ? 
      { discipline: disciplineId, email: user?.email } : 
      { disciplineName };
    api
      .get('/package', {
        params
      })
      .then((response) => {
        const { data } = response;
        setDisciplinePacks(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
      });
  };
  const deletePackage = (id) => {
    if(!user || !user.email) return;
    setLoading(true);
    const { email } = user;
    const pack = {
      discipline: disciplineId,
      email,
      id,
    };
    api.delete('/package', { params: pack })
      .then((response) => {
        const {status, data} = response;
        if (status === 200) {
          const id = data;
          setDisciplinePacks(disciplinePacks.filter(x => x.id !== id));
        }
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
      });
  };
  const buyPack = (pack, selectedSpot) => {
    if(!user || !user.email) return;
    setLoading(true);
    const { email } = user;
    const payload = {
      email,
      pack,
      selectedSpot
    };
    api.post('/class', payload)
      .then((response) => {
        updateUser({
          credits: user?.credits ? user.credits - pack.value : 0,
        });
        setLoading(false);
        navigate('/disciplinas');
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
        setSnackbar('Saldo insuficiente');
      });
  };
  const handlePackUpdate = (newPack: disciplinePacks) => {
    const oldPacks = disciplinePacks.filter(x => x.id !== newPack.id);
    setDisciplinePacks([
      ...oldPacks,
      newPack
    ]);
  };
  useEffect(() => getPackage(), [user]);

  const emptyPacksProfessor = () => (
    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
      <h1>{disciplinePrettyName.toUpperCase()}</h1>
      <h2>Vishe! Isso aqui está vazio! Adicione um novo pacote clicando em Adicionar.</h2>
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', minWidth: '200px'}}>
        <Button onClick={() => setShowPacksForm(true)}>Adicionar</Button>
        <Button onClick={() => {
          navigate('/disciplinas', {
            replace: true,
          });
        }}>Voltar</Button>
      </div>
    </div>
  );

  const emptyPacksAluno = () => (
    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
      <h1>{disciplinePrettyName.toUpperCase()}</h1>
      <h2>Vishe! Não encontramos nenhum pacote para a disciplina selecionada!.</h2>
      <Button sx={{width: '20%'}} onClick={() => {
        navigate('/disciplinas', {
          replace: true,
        });
      }}>Voltar</Button>
    </div>
  );

  const addPack = () => {
    return (
      <PackageForm
        email={user?.email || ''}
        discipline={disciplineId}
        setShowPacksForm={setShowPacksForm}
        setLoading={setLoading}
        setDisciplinePacks={handlePackUpdate}
        selectedPack={selectedPack}
        disciplineName={disciplineName}
      />
    );
  };

  const listPacksProfessor = () => (
    <>
      <h1>{disciplinePrettyName.toUpperCase()}</h1>
      <List style={{maxHeight: '500px', overflow: 'auto', display: 'flex', flexWrap: 'wrap', justifyContent: 'center', minWidth: '600px'}}>
        {
          disciplinePacks.map(
            pack => (
              <PackCard 
                key={pack.id} 
                pack={pack}
                onRemove={deletePackage}
                onVisualize={() => {
                  setSelectedPack(pack);
                  setShowPacksForm(true);
                }}
              />
            )
          )
        }
      </List>
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', minWidth: '200px'}}>
        <Button onClick={() => {
          setSelectedPack(undefined);
          setShowPacksForm(true);
        }}>Adicionar</Button>
        <Button onClick={() => {
          navigate('/disciplinas', {
            replace: true,
          });
        }}>Voltar</Button>
      </div>
    </>
  );

  const listPacksAluno = () => (
    <>
      <h1>{disciplinePrettyName.toUpperCase()}</h1>
      <List style={{maxHeight: '500px', overflow: 'auto', display: 'flex', flexWrap: 'wrap'}}>
        {
          disciplinePacks.filter(pack => {
            return pack.agenda.some(spot => !spot.filled);
          }).map(
            pack => (
              <PackCard 
                key={pack.id} 
                pack={pack}
                onRemove={() => { null; }}
                onVisualize={(a, b) => { buyPack(a, b); }}
              />
            )
          )
        }
      </List>
      <Button onClick={() => {
        navigate('/disciplinas', {
          replace: true,
        });
      }}>Voltar</Button>
    </>
  );

  const getCurrentView = () => {
    if (loading) {
      return <CircularProgress size={200}/>;
    }
    if (user?.userType === 'professor') {
      if (disciplinePacks.length && !showPacksForm) {
        return listPacksProfessor();
      }
      if (showPacksForm) {
        return addPack();
      }
      return emptyPacksProfessor();
    } else {
      if (disciplinePacks.length && !showPacksForm) {
        return listPacksAluno();
      }
      return emptyPacksAluno();
    }
  };

  return (
    <div className="packages">
      {getCurrentView()}
      <Snack open={Boolean(snackbar)} onClose={setSnackbar} severity="error" message={snackbar} />
    </div>
  );
};
