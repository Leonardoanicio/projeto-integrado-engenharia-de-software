import { useRef } from 'react';

type timeoutType = {
  current?: NodeJS.Timeout;
};
export const useDebounce = (fnc: React.Dispatch<React.SetStateAction<string>>, delay: number) => {
  const timeoutRef: undefined | timeoutType = useRef(undefined);

  return (arg: string) => {
    clearTimeout(timeoutRef.current);

    timeoutRef.current = setTimeout(() => {
      fnc(arg);
    }, delay);
  };
};
