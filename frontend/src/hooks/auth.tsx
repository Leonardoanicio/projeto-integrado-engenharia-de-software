import React, { createContext, useCallback, useState, useContext, useEffect } from 'react';
import jwt_decode from 'jwt-decode';
import api from '../services/api';
import { cognitoGetUserType, cognitoSignIn, cognitoSignUp } from '../services/cognito';
import { UserNotFound } from '../errors/UserNotFound';

type userType = {
  email?: string;
  userType?: string;
  active?: boolean;
  accountDeleted?: boolean;
  name?: string;
  document?: string;
  cellphone?: string;
  address?: { [key: string]: string | undefined };
  profilePicture?: string;
  credits?: number;
};

type dataType = {
  token?: string;
  accessToken?: string;
  user?: userType;
};

type decodeTokenType = {
  exp?: number;
};

type signInUserType = {
  password: string;
  email: string;
  create?: boolean;
};

type SignInResponse = {
  data?: userType;
  error?: string;
};

type signUpUserType = {
  password: string;
  email: string;
  userType: string;
};

type useAuthType = {
  signUp: (user: signUpUserType) => Promise<string | undefined>;
  signIn: (user: signInUserType) => Promise<SignInResponse>;
  signOut: () => void;
  updateUser: (user: userType) => void;
  loading: boolean;
  token?: string;
  user?: userType;
  accessToken?: string;
};

const AuthContext = createContext<useAuthType | undefined>(undefined);

export const AuthProvider = ({ children }: { children: React.ReactNode }) => {
  const [data, setData] = useState<dataType>();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    function loadStoragedData() {
      const token = localStorage.getItem('@Pingo:token');
      const user = localStorage.getItem('@Pingo:user');
      const accessToken = localStorage.getItem('@Pingo:accessToken');

      if (token && user && accessToken) {
        const decodeToken = jwt_decode<decodeTokenType>(token);
        const now = new Date().getTime() / 1000;
        if (!decodeToken.exp || decodeToken.exp < now) {
          localStorage.removeItem('@Pingo:token');
          localStorage.removeItem('@Pingo:user');
          localStorage.removeItem('@Pingo:accessToken');
          setData(undefined);
        } else {
          api.defaults.headers.common['Authorization'] = `${token}`;
          setData({ token: token, user: JSON.parse(user), accessToken: accessToken });
        }
      } else {
        localStorage.removeItem('@Pingo:token');
        localStorage.removeItem('@Pingo:user');
        localStorage.removeItem('@Pingo:accessToken');
      }
      setLoading(false);
    }

    loadStoragedData();
  }, []);

  const signIn = useCallback(
    async ({ email, password, create = false }: signInUserType): Promise<SignInResponse> => {
      try {
        const {
          userAuth: { token, accessToken, error },
          cognitoUser
        } = await cognitoSignIn(email, password);
        if (error) {
          throw new Error(error);
        }
        if (!token || !accessToken) {
          throw new UserNotFound();
        }

        api.defaults.headers.common['Authorization'] = `${token}`;
        if (create) {
          const userType = await cognitoGetUserType(cognitoUser);
          await api.post(`/users/${email}`, {
            userType
          });
        }
        const response = await api.get<userType>(`/users/${email}`);
        const user = response.data;
        if (!user) {
          throw new UserNotFound();
        }

        localStorage.setItem('@Pingo:token', token);
        localStorage.setItem('@Pingo:accessToken', accessToken);
        localStorage.setItem('@Pingo:user', JSON.stringify(user));

        setData({ user, token, accessToken });
        return { data: response.data };
      } catch (err) {
        console.log('Erro: ', err);
        return { error: (err as Error).message };
      }
    },
    []
  );

  const signUp = useCallback(async ({ email, password, userType }: signUpUserType) => {
    try {
      const cognitoUserRequest = await cognitoSignUp(email, password, userType);
      if (!cognitoUserRequest || !cognitoUserRequest.userConfirmed) {
        throw new UserNotFound();
      }
      return undefined;
    } catch (err) {
      return (err as Error).message;
    }
  }, []);

  const signOut = useCallback(() => {
    localStorage.removeItem('@Pingo:token');
    localStorage.removeItem('@Pingo:user');
    api.defaults.headers.common['Authorization'] = '';

    setData(undefined);
  }, []);

  const updateUser = (updateMap: userType) => {
    if (data?.user) {
      const newUser = {
        ...data.user,
        ...updateMap
      };
      setData({ token: data?.token, user: newUser, accessToken: data?.accessToken });
      localStorage.setItem('@Pingo:user', JSON.stringify(newUser));
    }
  };

  return (
    <AuthContext.Provider
      value={{
        user: data?.user,
        token: data?.token,
        accessToken: data?.accessToken,
        signUp,
        signIn,
        signOut,
        updateUser,
        loading
      }}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext<useAuthType | undefined>(AuthContext);
  if (!context) {
    throw new Error('useAuth must be use within an AuthProvider');
  }

  return context;
}
