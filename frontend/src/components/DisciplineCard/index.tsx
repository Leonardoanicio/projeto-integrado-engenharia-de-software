import './styles.css';
import React from 'react';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActions } from '@mui/material';
import { useAuth } from '../../hooks/auth';

type PropsType = {
  name: string;
  basename: string;
  id: string;
  onRemove: (args) => void;
  onVisualize: () => void;
};

export const DisciplineCard = (props: PropsType) => {
  const { user } = useAuth();
  const buttonSx = {
    fontSize: '0.75rem',
    backgroundColor: 'white',
    color: 'var(--main-text-gray) !important',
    '&:hover': {
      backgroundColor: 'var(--main-gray) !important',
    },
    borderRadius: '0 !important'
  };

  return (
    <Card sx={{ maxWidth: '250px', maxHeight: '250px' }} className="DisciplineCard">
      <CardMedia
        component="img"
        height="140"
        image='lesson_image.jpg'
        onError={(e) => {e.target.src = 'lesson_image.jpg';}}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {props.name[0].toUpperCase() + props.name.slice(1)}
        </Typography>
      </CardContent>
      <CardActions>
        <Button sx={buttonSx} size="small" color="primary" onClick={props.onVisualize}>
          {user?.userType === 'professor' ? 'Detalhes' : 'Agenda'}
        </Button>
        <Button sx={buttonSx} size="small" color="primary" onClick={() => props.onRemove(props.id)}>
          Remover
        </Button>
      </CardActions>
    </Card>
  );
};
