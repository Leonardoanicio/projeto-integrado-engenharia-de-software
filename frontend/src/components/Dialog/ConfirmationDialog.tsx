import React from 'react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from '@mui/material';
import { Button } from '@mui/material';

type PropsType = {
  open: boolean;
  handleClose: () => Promise<void> | void;
  handleConfirm: () => Promise<void> | void;
  title: string;
  content: string;
};

export const ConfirmationDialog = (props: PropsType) => (
  <Dialog
    open={props.open}
    onClose={props.handleClose}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description">
    <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">{props.content}</DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button
        onClick={props.handleClose}
        sx={{
          fontSize: '1rem'
        }}>
        Voltar
      </Button>
      <Button
        onClick={props.handleConfirm}
        autoFocus
        sx={{
          fontSize: '1rem'
        }}>
        Confirmar
      </Button>
    </DialogActions>
  </Dialog>
);
