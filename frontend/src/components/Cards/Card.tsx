import './Card.css';
import React from 'react';

type PropsType = {
  background: string;
  textBackground: string;
  textColor: string;
  cardHeader: string;
  img: string;
  cardText: string;
};

export const Card = (props: PropsType) => {
  const cardStyle = {
    background: props.background
  };

  const textStyle = {
    background: props.textBackground,
    color: props.textColor
  };

  return (
    <div className="Card">
      <b>{props.cardHeader}</b>
      <div className="Content" style={cardStyle}>
        <div className="ImageArea">
          <img src={props.img} />
        </div>
        <div className="TextArea" style={textStyle}>
          <p>{props.cardText}</p>
        </div>
      </div>
    </div>
  );
};
