import './styles.css';
import React from 'react';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Button,  CardActions } from '@mui/material';
import { useAuth } from '../../hooks/auth';
import * as Mui from '@mui/material';
import MonetizationOnRoundedIcon from '@mui/icons-material/MonetizationOnRounded';

type disciplinePacks = {
  id: string;
  packageName: string;
  owner: string;
  details: string;
  value: number;
  lessons: number;
  user: {
    picture: string;
    name: string;
  };
  agenda: Array<any>;
}

type PropsType = {
  pack: disciplinePacks;
  onRemove: (args) => void;
  onVisualize: (...args) => void;
};

export const PackCard = ({pack, onRemove, onVisualize}: PropsType) => {
  const { user } = useAuth();
  const [dialog, setDialog] = React.useState(false);
  const [selectedSpot, setSelectedSpot] = React.useState<number>(0);

  const buttonSx = {
    fontSize: '0.75rem',
    backgroundColor: 'white',
    color: 'var(--main-text-gray) !important',
    '&:hover': {
      backgroundColor: 'var(--main-gray) !important',
    },
    '&:disabled': {
      backgroundColor: 'white !important',
      color: 'white !important',
    }
  };

  return (
    <Card className="package-card">
      <div style={{display: 'flex', 'flexDirection': 'row', background: 'var(--main-gray)'}}>
        <Typography 
          sx={{ fontSize: 20, fontWeight: 'bold',  width: '80%', padding: '16px 0 16px 10px' }}  
        >
          {pack.packageName[0].toUpperCase() + pack.packageName.slice(1)}
        </Typography>
        <img src={pack.user.picture} className="picture" />
      </div>
      <CardContent sx={{ flex: 1}}>
        <Typography 
          variant="body2" 
          component="div" 
          sx={{color: 'var(--main-text-gray)'}}
        >
          {pack.details}
        </Typography>
      </CardContent>
      <CardContent>
        <Typography 
          component="div" 
          sx={{ fontSize: 14, color: 'var(--main-text-gray)'}}  
        >
          <div  style={{display: 'flex', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
              Valor: {pack.value} 
            <MonetizationOnRoundedIcon
              style={{
                fontSize: '1.25rem',
                color: 'var(--secondary-yellow)'
              }}
            />
          </div>
          Aulas: {pack.lessons}
        </Typography>
      </CardContent>
      {
        user && user.userType === 'professor' ?
          (
            <CardActions>
              <Button sx={buttonSx} size="small" color="primary" onClick={onVisualize}>
                Editar
              </Button>
              <Button sx={buttonSx} size="small" color="primary" disabled={pack.agenda.some((spot) => spot.filled)} onClick={() => onRemove(pack.id)}>
                Remover
              </Button>
            </CardActions>
          ) :
          (
            <CardActions>
              <Button sx={buttonSx} size="small" color="primary" onClick={() => setDialog(true)}>
                Comprar
              </Button>
            </CardActions>
          )
      }
      <Mui.Dialog
        open={dialog}
        onClose={() => setDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <Mui.DialogTitle id="alert-dialog-title"></Mui.DialogTitle>
        <Mui.DialogContent>
          <Mui.DialogContentText id="alert-dialog-description"> 
            Selecione o horário que deseja agendar a aula: <br></br>
            <Mui.Select variant="standard" value={selectedSpot} onChange={({target}) => setSelectedSpot(parseInt(String(target.value)))}>
              {
                pack.agenda.filter(spot => !spot.filled).map((spot, index) => (
                  <Mui.MenuItem key={index} value={index}>
                    {spot.day} - {spot.begin} às {spot.end}
                  </Mui.MenuItem>
                ))
              }
            </Mui.Select>
          </Mui.DialogContentText>
        </Mui.DialogContent>
        <Mui.DialogActions>
          <Button
            onClick={() => setDialog(false)}
            sx={{
              fontSize: '1rem'
            }}>
            Voltar
          </Button>
          <Button
            onClick={() => onVisualize(pack, selectedSpot)}
            autoFocus
            sx={{
              fontSize: '1rem'
            }}>
            Confirmar
          </Button>
        </Mui.DialogActions>
      </Mui.Dialog>
    </Card>
  );
};
