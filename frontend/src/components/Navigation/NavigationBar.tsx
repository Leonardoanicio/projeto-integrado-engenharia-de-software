import React, { useState } from 'react';
import './NavigationBar.css';
import { useNavigate } from 'react-router-dom';

import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MonetizationOnRoundedIcon from '@mui/icons-material/MonetizationOnRounded';

import { useAuth } from '../../hooks/auth';

const button_list = ['Perfil', 'Foto', 'Conta', 'Pingos', 'Agenda', 'Disciplinas', 'Logout'];

export const NavigationBar = () => {
  const { user, signOut } = useAuth();
  const navigate = useNavigate();

  const [selectedIndex, setSelectedIndex] = useState<string>(window.location.pathname);

  const handleListItemClick = (e: string) => {
    setSelectedIndex(e);

    if (e === '/logout') {
      signOut();
      navigate('../', {
        replace: true
      });
    } else {
      navigate(e, {
        replace: true
      });
    }
  };

  const sx_list = {
    width: '100%'
  };

  const sx_text = (index: string) => {
    return {
      color: selectedIndex === index ? 'white' : 'gray'
    };
  };

  return (
    <div className="navbar">
      <div className="picture">
        {user?.profilePicture ? (
          <img src={user.profilePicture} className="ProfilePicture" />
        ) : (
          <AccountCircleOutlinedIcon className="empty_picture" />
        )}
        <span style={{ fontSize: '2rem' }}>{user?.name ? user.name.split(' ')[0] : '\u00A0'}</span>
        <div
          style={{
            display: 'flex',
            alignItems: 'center'
          }}>
          <span style={{ fontSize: '1.5rem' }}>{user?.credits ?? 0} </span>
          <MonetizationOnRoundedIcon
            style={{
              fontSize: '2rem',
              color: 'gold'
            }}
          />
        </div>
      </div>
      <List component="nav" sx={sx_list}>
        {button_list.map((button, index) => {
          const button_url = `/${button.toLowerCase()}`;
          return (
            <ListItemButton
              key={index}
              selected={selectedIndex === button_url}
              onClick={() => handleListItemClick(button_url)}>
              <ListItemText primaryTypographyProps={sx_text(button_url)} primary={button} />
            </ListItemButton>
          );
        })}
      </List>
    </div>
  );
};
