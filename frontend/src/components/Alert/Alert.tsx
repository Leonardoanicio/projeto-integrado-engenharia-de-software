import * as React from 'react';
import MuiAlert, { AlertProps, AlertColor } from '@mui/material/Alert';
import { Snackbar } from '@mui/material';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

type PropsType = {
  open: boolean;
  severity: AlertColor;
  message: string | undefined;
  onClose: React.Dispatch<React.SetStateAction<string | undefined>>;
};

export const Snack = (props: PropsType) => {
  const handleClose = () => {
    props.onClose(undefined);
  };
  return (
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      sx={{ marginTop: '8vh' }}
      open={Boolean(props.open)}
      onClose={handleClose}
      autoHideDuration={5000}>
      <Alert severity={props.severity} sx={{ width: '100%' }}>
        {props.message}
      </Alert>
    </Snackbar>
  );
};
