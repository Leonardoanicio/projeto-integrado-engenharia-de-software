import './LoginButton.css';
import React from 'react';
import { LoadingButton } from '@mui/lab';
import { useNavigate } from 'react-router-dom';

import { useAuth } from '../../../hooks/auth';

type LoginButtonPropsType = {
  email: string;
  password: string;
  active: boolean;
};

export const LoginButton = (props: LoginButtonPropsType) => {
  const { signIn } = useAuth();
  const navigate = useNavigate();
  const [loading, setLoading] = React.useState(false);

  const handleClick = async () => {
    setLoading(true);
    const logged = await signIn({
      email: props.email,
      password: props.password
    });

    setLoading(false);
    if (logged) {
      navigate('/perfil', {
        replace: true
      });
    }
  };

  const sx = {
    '&.Mui-disabled': {
      color: 'white',
      fontWeight: 'bold'
    }
  };

  return (
    <LoadingButton className="LoginButton" loading={loading} disabled={!props.active} onClick={handleClick} sx={sx}>
      Login
    </LoadingButton>
  );
};
