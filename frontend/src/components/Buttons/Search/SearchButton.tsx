import './SearchButton.css';
import React from 'react';
import Button from '@mui/material/Button';

export const SearchButton = () => {
  return (
    <div>
      <Button className="SearchButton"> Buscar</Button>
    </div>
  );
};
