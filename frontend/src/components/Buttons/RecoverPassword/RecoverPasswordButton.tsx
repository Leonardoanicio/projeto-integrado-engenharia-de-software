import './RecoverPasswordButton.css';
import React from 'react';
import Button from '@mui/material/Button';

type RecoverPasswordButtonType = {
  active: boolean;
  email: string;
};

export const RecoverPasswordButton = (props: RecoverPasswordButtonType) => {
  const handleClick = () => {
    console.log(props);
  };

  return (
    <Button className="LoginButton" disabled={!props.active} onClick={handleClick}>
      {' '}
      Recuperar Senha{' '}
    </Button>
  );
};
