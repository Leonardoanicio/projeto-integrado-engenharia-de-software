import React from 'react';
import './RadioButton.css';

import { RadioGroup, Radio, FormControl, FormControlLabel } from '@mui/material';
import { ReactEventType } from '../../../types/commonType';

type RadioButtonPropsType = {
  value: string;
  name: string;
  radios: {
    value: string;
    label: string;
  }[];
  onChange: (param: string) => Promise<void> | void;
};

export const RadioButton = (props: RadioButtonPropsType) => {
  const [value, setValue] = React.useState(props.value);

  const handleChange = ({ target: { value } }: ReactEventType) => {
    setValue(value);
    props.onChange(value);
  };

  return (
    <div className="RadioButton">
      <FormControl>
        <RadioGroup
          row
          name={props.name}
          value={value}
          defaultValue={value}
          onChange={handleChange}
          style={{ display: 'flex' }}>
          {props.radios.map((radio, index) => (
            <FormControlLabel
              value={radio.value}
              control={<Radio />}
              label={radio.label}
              key={index}
            />
          ))}
        </RadioGroup>
      </FormControl>
    </div>
  );
};
