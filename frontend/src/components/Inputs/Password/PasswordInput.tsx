import './PasswordInput.css';
import React, { useState, useEffect } from 'react';
import { PasswordValidation } from '../../../utils';
import { useDebounce } from '../../../hooks/debounce';

import IconButton from '@mui/material/IconButton';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

import { FormControl, FormHelperText } from '@mui/material';
import { ReactEventType } from '../../../types/commonType';
import { PasswordType } from '../../../types/passwordType';

export const PasswordInput = (props: PasswordType) => {
  const { password, setPassword } = props;
  const [displayPassword, setDisplayPassword] = useState<string>(password);
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const debounceChange = useDebounce(setPassword, 250);

  const formatWarning = (warnings: string[]) => {
    return (
      <ul>
        {warnings.map((warning) => (
          <li key={warning}>{warning}</li>
        ))}
      </ul>
    );
  };

  const handlePasswordChange = ({ target: { value } }: ReactEventType) => {
    setDisplayPassword(value);
    debounceChange(value);
  };

  const handleKeyPress = ({ key }: React.KeyboardEvent) => {
    if (key === 'Enter' && props.onEnter) {
      props.onEnter();
    }
  };

  useEffect(() => {
    if (props?.setWarning) {
      const validationResults = PasswordValidation(password);
      password === '' || validationResults.length === 0
        ? props.setWarning('')
        : props.setWarning(formatWarning(validationResults));
    }
    if (password === '') {
      setDisplayPassword('');
    }
  }, [password]);

  return (
    <FormControl>
      <OutlinedInput
        id={`${props.placeholder}_input`}
        onChange={handlePasswordChange}
        onKeyUp={handleKeyPress}
        type={showPassword ? 'text' : 'password'}
        placeholder={props.placeholder}
        value={displayPassword}
        endAdornment={
          <InputAdornment position="end">
            <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        }
        sx={props?.sx}
      />
      <FormHelperText>{props.warning}</FormHelperText>
    </FormControl>
  );
};
