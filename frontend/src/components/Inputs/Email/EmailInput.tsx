import './EmailInput.css';
import React, { useEffect, useState } from 'react';
import { EmailValidation } from '../../../utils';
import { useDebounce } from '../../../hooks/debounce';
import OutlinedInput from '@mui/material/OutlinedInput';
import { FormControl, FormHelperText } from '@mui/material';
import { ReactEventType } from '../../../types/commonType';
import { EmailType } from '../../../types/emailType';

export const EmailInput = (props: EmailType) => {
  const { email, setEmail } = props;
  const [displayEmail, setDisplayEmail] = useState<string>(email);

  const debounceChange = useDebounce(setEmail, 250);

  const handleChange = ({ target: { value } }: ReactEventType) => {
    setDisplayEmail(value);
    debounceChange(value);
  };
  
  const handleKeyPress = ({ key }: React.KeyboardEvent) => {
    if (key === 'Enter' && props.onEnter) {
      props.onEnter();
    }
  };


  useEffect(() => {
    if (props?.setWarning) {
      const validationResults = EmailValidation(email);
      email === '' || validationResults === ''
        ? props.setWarning('')
        : props.setWarning(validationResults);
    }
  }, [email]);

  return (
    <FormControl>
      <OutlinedInput
        id="email_input"
        onChange={handleChange}
        type="email"
        placeholder={props.placeholder ?? 'Email'}
        value={displayEmail}
        onKeyUp={handleKeyPress}
      />
      <FormHelperText>{props.warning}</FormHelperText>
    </FormControl>
  );
};
