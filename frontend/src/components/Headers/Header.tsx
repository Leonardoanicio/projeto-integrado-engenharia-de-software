import React from 'react';
import './Header.css';
import pingo from '../../images/pingo.png';

import { Link } from 'react-router-dom';
import { useAuth } from '../../hooks/auth';

import Button from '@mui/material/Button';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';

export const Header = () => {
  const { user, token } = useAuth();

  return (
    <div className="Header">
      <div className="SubHeader">
        <Link to="/">
          <img src={pingo} />
        </Link>
        <div className="Logo">
          <h1>pingo</h1>
          <h2>Sua plataforma de aprendizagem</h2>
        </div>
      </div>
      {user && token ? (
        <div className="SubHeader">
          <Link to="/agenda" className="Link">
            <Button className="HeaderButton">Minha agenda</Button>
          </Link>
          <Link to="/disciplinas" className="Link">
            <Button className="HeaderButton">Minhas disciplinas</Button>
          </Link>
          <Link to="/perfil" className="Link">
            <Button
              className="HeaderButtonNoBorder"
              style={user.profilePicture ? { padding: 0 } : undefined}>
              {user?.profilePicture ? (
                <img src={user.profilePicture} className="ProfilePicture" />
              ) : (
                <AccountCircleOutlinedIcon className="EmptyProfilePicture" />
              )}
            </Button>
          </Link>
        </div>
      ) : (
        <div className="SubHeader">
          <Link to="/cadastro" className="Link">
            <Button className="HeaderButton">Quero dar aulas</Button>
          </Link>
          <Link to="/login" className="Link">
            <Button className="HeaderButtonNoBorder">Login</Button>
          </Link>
          <Link to="/cadastro" className="Link">
            <Button className="HeaderButton">Cadastre-se</Button>
          </Link>
        </div>
      )}
    </div>
  );
};
