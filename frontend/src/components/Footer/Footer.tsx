import React from 'react';
import './Footer.css';

import { FacebookRounded, Instagram, Twitter } from '@mui/icons-material';

import { Link } from 'react-router-dom';

export const Footer = () => (
  <div className="Footer">
    <div className="SubHeaderLeft">
      <Link to="/sobre" className="Link">
        Sobre
      </Link>
      <Link to="/suporte" className="Link">
        Suporte
      </Link>
    </div>
    <div className="SubHeaderRight">
      <FacebookRounded className="Icon" />
      <Instagram className="Icon" />
      <Twitter className="Icon" />
    </div>
  </div>
);
