import { Entity } from 'dynamodb-toolbox';
import { MainTable } from '../database/pingoTable';

type appointmentType = {
  id: string;
  aluno: string;
  professor: string;
  beginsAt: string;
  endsAt: string;
  classId: string;
  details: string;
};

type CustomCompositeKey = {
  aluno?: string;
  professor?: string;
};

export const appointmentModel = new Entity<appointmentType, CustomCompositeKey, typeof MainTable>({
  name: 'APPOINTMENT',
  timestamps: true,

  attributes: {
    pk: { type: 'string', hidden: true, partitionKey: true, default: 'APPOINTMENT' },
    sk: { type: 'string', hidden: true, sortKey: true },
    id: ['sk', 0, { type: 'string', required: true }],
    aluno: { type: 'string', required: true },
    professor: { type: 'string', required: true },
    beginsAt: { type: 'string', required: true },
    endsAt: { type: 'string', required: true },
    classId: { type: 'string', required: true },
    details: { type: 'string', required: true }
  },

  table: MainTable
});
