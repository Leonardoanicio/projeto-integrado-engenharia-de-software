import { Entity } from 'dynamodb-toolbox';
import { TwilioTable } from '../database/twilioTable';

export type roomType = {
  RoomName?: string;
  RoomSid?: string;
  RoomType?: string;
  BeginsAt?: string;
  participants: string[];
};

type CustomCompositeKey = {
  RoomName: string;
};

export const roomModel = new Entity<roomType, CustomCompositeKey, typeof TwilioTable>({
  name: 'ROOM',
  timestamps: true,

  attributes: {
    pk: { type: 'string', partitionKey: true, default: 'ROOM' },
    sk: { type: 'string', hidden: true, sortKey: true },
    RoomName: ['sk', 0, { type: 'string', required: true }],
    RoomSid: { type: 'string', required: true },
    RoomType: { type: 'string', required: true },
    BeginsAt: { type: 'string', required: true },
    participants: { type: 'set', required: true }
  },

  table: TwilioTable
});
