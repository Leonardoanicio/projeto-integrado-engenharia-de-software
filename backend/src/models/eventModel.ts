import { Entity } from 'dynamodb-toolbox';
import { TwilioTable } from '../database/twilioTable';

export type eventType = {
  id: string;
  RoomName?: string;
  RoomSid?: string;
  RoomType?: string;
  RoomStatus?: string;
  StatusCallbackEvent?: string;
  Timestamp?: string;
  ParticipantSid?: string;
  ParticipantIdentity?: string;
  ParticipantStatus?: string;
  ParticipantDuration?: number;
  RoomDuration?: number;
};

type CustomCompositeKey = {
  RoomName?: string;
  RoomSid?: string;
};

export const eventModel = new Entity<eventType, CustomCompositeKey, typeof TwilioTable>({
  name: 'EVENT',
  timestamps: true,

  attributes: {
    pk: { type: 'string', partitionKey: true, default: 'EVENT' },
    sk: { type: 'string', hidden: true, sortKey: true },
    id: ['sk', 0, { type: 'string', required: true }],
    RoomName: { type: 'string', required: true },
    RoomSid: { type: 'string', required: true },
    RoomType: { type: 'string', required: false },
    RoomStatus: { type: 'string', required: false },
    StatusCallbackEvent: { type: 'string', required: false },
    Timestamp: { type: 'string', required: false },
    ParticipantSid: { type: 'string', required: false },
    ParticipantIdentity: { type: 'string', required: false },
    ParticipantStatus: { type: 'string', required: false },
    ParticipantDuration: { type: 'number', required: false },
    RoomDuration: { type: 'number', required: false }
  },

  table: TwilioTable
});
