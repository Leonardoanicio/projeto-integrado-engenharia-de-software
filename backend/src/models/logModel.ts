import { Entity } from 'dynamodb-toolbox';
import { MainTable } from '../database/pingoTable';

export type logType = {
  id: string;
  token: string;
  user: string;
  date: string;
  payload: string;
  endpoint: string;
};

type CustomCompositeKey = {
  user: string;
};

export const logModel = new Entity<logType, CustomCompositeKey, typeof MainTable>({
  name: 'LOG',
  timestamps: true,

  attributes: {
    pk: { type: 'string', partitionKey: true, default: 'LOG' },
    sk: { type: 'string', hidden: true, sortKey: true },
    id: ['sk', 0, { type: 'string', required: true }],
    token: { type: 'string', required: true },
    user: { type: 'string', required: true },
    date: { type: 'string', required: true },
    payload: { type: 'string', required: true },
    endpoint: { type: 'string', required: true }
  },

  table: MainTable
});
