import { Entity } from 'dynamodb-toolbox';
import { MainTable } from '../database/pingoTable';

type packageType = {
  disciplineId: string;
  id?: string;
  packageName?: string;
  owner?: string;
  details?: string;
  value?: number;
  lessons?: number;
  lessonDuration?: number;
  disciplineName?: string;
  agenda?: Array<any>;
};

type CustomCompositeKey = {
  disciplineId: string;
  owner: string;
  id: string;
};

export const packageModel = new Entity<packageType, CustomCompositeKey, typeof MainTable>({
  name: 'PACKAGE',
  timestamps: true,

  attributes: {
    pk: { type: 'string', partitionKey: true, default: 'PACKAGE' },
    sk: { type: 'string', hidden: true, sortKey: true },
    disciplineId: ['sk', 0, { type: 'string', required: true }],
    owner: ['sk', 1, { type: 'string', required: true }],
    id: ['sk', 2, { type: 'string', required: true }],
    packageName: { type: 'string' },
    details: { type: 'string', required: true },
    value: { type: 'number', required: true },
    lessons: { type: 'number', required: true },
    agenda: { type: 'list', required: true },
    disciplineName: { type: 'string', required: true }
  },

  table: MainTable
});
