import { Entity } from 'dynamodb-toolbox';
import { MainTable } from '../database/pingoTable';

type userType = {
  email: string;
  name?: string;
  document?: string;
  cellphone?: string;
  address?: { [key: string]: string };
  userType?: string;
  active?: boolean;
  accountDeleted?: boolean;
  profilePicture?: string;
  credits?: number;
};

type CustomCompositeKey = {
  email: string;
};

export const userModel = new Entity<userType, CustomCompositeKey, typeof MainTable>({
  name: 'USER',
  timestamps: true,

  attributes: {
    email: { type: 'string', partitionKey: true },
    name: { type: 'string' },
    document: { type: 'string' },
    cellphone: { type: 'string' },
    address: { type: 'map' },
    userType: { type: 'string', required: true },
    sk: { type: 'string', default: 'PROFILE', hidden: true, sortKey: true },
    active: { type: 'boolean', default: true, required: true },
    accountDeleted: { type: 'boolean', default: false, required: true },
    profilePicture: { type: 'string' },
    credits: { type: 'number', default: 0 }
  },

  table: MainTable
});
