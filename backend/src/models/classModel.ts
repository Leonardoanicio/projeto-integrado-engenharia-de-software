import { Entity } from 'dynamodb-toolbox';
import { MainTable } from '../database/pingoTable';

type classType = {
  email_aluno: string;
  disciplineId: string;
  id: string;
  email_professor: string;
  name: string;
  details: string;
  value: number;
  lessons: number;
  disciplineName: string;
  packId: string;
  spotNumber: number;
};

type CustomCompositeKey = {
  disciplineId: string;
  email_aluno: string;
  id: string;
};

export const classModel = new Entity<classType, CustomCompositeKey, typeof MainTable>({
  name: 'CLASS',
  timestamps: true,

  attributes: {
    pk: { type: 'string', partitionKey: true, default: 'CLASS' },
    sk: { type: 'string', hidden: true, sortKey: true },
    email_aluno: ['sk', 0, { type: 'string', required: true }],
    disciplineId: ['sk', 1, { type: 'string', required: true }],
    id: ['sk', 2, { type: 'string', required: true }],
    email_professor: { type: 'string', required: true },
    name: { type: 'string' },
    details: { type: 'string', required: true },
    value: { type: 'number', required: true },
    lessons: { type: 'number', required: true },
    disciplineName: { type: 'string', required: true },
    packId: { type: 'string', required: true },
    spotNumber: { type: 'number', required: true }
  },

  table: MainTable
});
