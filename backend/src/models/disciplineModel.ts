import { Entity } from 'dynamodb-toolbox';
import { MainTable } from '../database/pingoTable';

type disciplineType = {
  email: string;
  id?: string;
  name?: string;
  packages?: string[];
};

type CustomCompositeKey = {
  email: string;
  id: string;
};

export const disciplineModel = new Entity<disciplineType, CustomCompositeKey, typeof MainTable>({
  name: 'DISCIPLINE',
  timestamps: true,

  attributes: {
    email: { type: 'string', partitionKey: true },
    sk: { type: 'string', hidden: true, sortKey: true },
    id: ['sk', 0, { type: 'string', prefix: 'DISCIPLINE', required: true }],
    name: { type: 'string' }
  },

  table: MainTable
});
