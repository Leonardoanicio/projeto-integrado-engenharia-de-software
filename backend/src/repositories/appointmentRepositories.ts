import { randomUUID } from 'crypto';
import { EntityItem } from 'dynamodb-toolbox';
import { appointmentModel } from 'src/models/appointmentModel';
import { MainTable } from 'src/database/pingoTable';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
type appointmentType = EntityItem<typeof appointmentModel>;

const days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];

const formatDate = (day, hour) => {
  const dayIndex = days.indexOf(day);
  const now = new Date();
  now.setDate(now.getDate() + ((dayIndex + 7 - now.getDay()) % 7));
  now.setHours(hour.split(':')[0]);
  now.setMinutes(hour.split(':')[1]);
  now.setHours(now.getHours() + 3);

  return now;
};

const wait = async (timeToWait: number) => {
  await new Promise((resolve) => setTimeout(resolve, timeToWait));
};

const writeAndRetry = async (
  items: {
    [key: string]: DocumentClient.WriteRequest;
  }[]
) => {
  let response = await MainTable.batchWrite(items, {
    execute: true,
    parse: true
  });
  let retry = 1;
  while (
    response.UnprocessedItems &&
    Object.keys(response.UnprocessedItems).length &&
    retry <= 15
  ) {
    await wait(1000);
    response = await response.next();
    retry++;
  }
};

export const createAppointment = async (
  classId: string,
  details: string,
  ammount: number,
  aluno: string,
  professor: string,
  time: any
) => {
  const { begin, end, day } = time;

  const beginDate = formatDate(day, begin);
  const endDate = formatDate(day, end);
  let appointments = [];
  for (let i = 0; i < ammount; i++) {
    const appointment = {
      id: randomUUID(),
      aluno,
      professor,
      beginsAt: beginDate.toISOString(),
      endsAt: endDate.toISOString(),
      classId: classId,
      details
    };
    const putBatch = appointmentModel.putBatch(appointment);
    appointments.push(putBatch);
    if (appointments.length >= 20) {
      await writeAndRetry(appointments);
      appointments = [];
    }

    beginDate.setDate(beginDate.getDate() + 7);
    endDate.setDate(endDate.getDate() + 7);
  }

  if (appointments.length) {
    await writeAndRetry(appointments);
  }
};

export const getAppointments = async (email) => {
  const query = {
    filters: [
      { attr: 'aluno' as keyof appointmentType, eq: email },
      { or: true, attr: 'professor' as keyof appointmentType, eq: email }
    ],
    startKey: null
  };

  let LastEvaluatedKey = null;
  let appointments = [];
  do {
    query.startKey = LastEvaluatedKey;
    const response = await appointmentModel.query('APPOINTMENT', query);
    LastEvaluatedKey = response.LastEvaluatedKey;
    appointments = [...appointments, ...(response.Items || [])];
  } while (LastEvaluatedKey);

  return appointments;
};

export const deleteAppointment = async ({ aluno, professor, classId }) => {
  const query = {
    filters: [
      { attr: 'aluno' as keyof appointmentType, eq: aluno },
      { attr: 'professor' as keyof appointmentType, eq: professor },
      { attr: 'classId' as keyof appointmentType, eq: classId }
    ],
    startKey: null
  };

  let LastEvaluatedKey = null;
  let appointments = [];
  do {
    query.startKey = LastEvaluatedKey;
    const response = await appointmentModel.query('APPOINTMENT', query);
    LastEvaluatedKey = response.LastEvaluatedKey;
    appointments = [...appointments, ...(response.Items || [])];
    console.log('Querying');
  } while (LastEvaluatedKey);

  for (const appointment of appointments) {
    await appointmentModel.delete(appointment);
  }
};

export const getTodayAppointments = async () => {
  const now = new Date().toISOString();
  const endOfToday = new Date();
  endOfToday.setHours(23, 59, 59, 999);
  const endOfTodayISO = endOfToday.toISOString();

  const query = {
    filters: [
      { attr: 'beginsAt' as keyof appointmentType, gt: now },
      { attr: 'beginsAt' as keyof appointmentType, lte: endOfTodayISO }
    ],
    startKey: null
  };

  let LastEvaluatedKey = null;
  let appointments = [];
  do {
    query.startKey = LastEvaluatedKey;
    const response = await appointmentModel.query('APPOINTMENT', query);
    LastEvaluatedKey = response.LastEvaluatedKey;
    appointments = [...appointments, ...(response.Items || [])];
  } while (LastEvaluatedKey);

  return appointments;
};
