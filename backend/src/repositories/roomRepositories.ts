import { roomModel, roomType } from '../models/roomModel';

export const putRoom = async (room: roomType) => {
  return roomModel.put(room);
};

export const updateRoomParticipants = async (roomName: string, user: string) => {
  return roomModel.update({
    RoomName: roomName,
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    participants: { $add: [user] }
  });
};
