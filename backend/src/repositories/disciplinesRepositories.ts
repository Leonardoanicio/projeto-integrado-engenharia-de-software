import { disciplineModel } from 'src/models/disciplineModel';
import type { EntityItem } from 'dynamodb-toolbox';

type disciplineType = EntityItem<typeof disciplineModel>;

export const findDiscipline = async ({ email, id }: disciplineType) => {
  const { Item } = await disciplineModel.get({ email, id });
  return Item;
};

export const listUserDisciplines = async ({ email }: disciplineType) => {
  const queryOptions = {
    beginsWith: 'DISCIPLINE'
  };
  let LastEvaluatedKey = null;
  let disciplines: disciplineType[] = [];
  do {
    const response = await disciplineModel.query(email, queryOptions);
    LastEvaluatedKey = response.LastEvaluatedKey;
    disciplines = [...disciplines, ...(response.Items || [])];
  } while (LastEvaluatedKey);
  return disciplines;
};

export const createDiscipline = async (discipline: disciplineType) => {
  await disciplineModel.put(discipline);
};

export const updateDiscipline = async (discipline: disciplineType) => {
  await disciplineModel.update(discipline);
};

export const deleteDiscipline = async ({ email, id }: disciplineType) => {
  await disciplineModel.delete({ email, id });
};
