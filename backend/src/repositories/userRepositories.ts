import { userModel } from '../models/userModel';
import type { EntityItem } from 'dynamodb-toolbox';

type userType = EntityItem<typeof userModel>;

export const findUser = async ({ email }: userType) => {
  const { Item } = await userModel.get({
    email
  });

  return Item;
};

export const createUser = async (user: userType) => {
  await userModel.put(user);
};

export const updateUser = async (user: userType) => {
  await userModel.update(user);
};
