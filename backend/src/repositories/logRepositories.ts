import { logModel, logType } from 'src/models/logModel';

export const putLog = async (log: logType) => {
  await logModel.put(log);
  return log;
};
