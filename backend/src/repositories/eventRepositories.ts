import { eventModel, eventType } from '../models/eventModel';

export const putEvent = async (event: eventType) => {
  return eventModel.put(event);
};
