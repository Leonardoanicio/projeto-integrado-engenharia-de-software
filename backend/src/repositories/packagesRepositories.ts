import { packageModel } from 'src/models/packageModel';
import type { EntityItem } from 'dynamodb-toolbox';
import { findUser } from './userRepositories';
import { revalidatePreSignedUrl } from 'src/utils/s3';

type packageType = EntityItem<typeof packageModel>;

export const findPackage = async ({ disciplineId, owner, id }: packageType) => {
  const { Item } = await packageModel.get({ disciplineId, owner, id });

  const user = await findUser({ email: owner });
  const profilePicture = user.profilePicture
    ? await revalidatePreSignedUrl(user.profilePicture)
    : null;
  return {
    ...Item,
    user: { picture: profilePicture, name: user.name }
  };
};

export const listPackages = async ({ disciplineId, disciplineName, owner }: packageType) => {
  const queryOptions =
    disciplineId || owner
      ? { beginsWith: disciplineId && owner ? `${disciplineId}#${owner}` : disciplineId }
      : { filters: { attr: 'disciplineName' as keyof packageType, eq: disciplineName } };

  let LastEvaluatedKey = null;
  let packages = [];
  do {
    const response = await packageModel.query('PACKAGE', queryOptions);
    LastEvaluatedKey = response.LastEvaluatedKey;
    packages = [...packages, ...(response.Items || [])];
  } while (LastEvaluatedKey);

  const userEmails = Array.from(new Set(packages.map((pack) => pack.owner)));
  const promises = Promise.all(
    userEmails.map(async (owner) => {
      const user = await findUser({ email: owner });
      return {
        email: user.email,
        name: user.name,
        picture: user.profilePicture ? await revalidatePreSignedUrl(user.profilePicture) : null
      };
    })
  );
  const profilePictures = await promises;
  return packages.map((pack) => {
    const { picture, name } = profilePictures.find((profile) => profile.email === pack.owner);
    return {
      ...pack,
      user: { picture, name }
    };
  });
};

export const createPackage = async (pack: packageType) => {
  await packageModel.put(pack);
};

export const updatePackage = async (pack: packageType) => {
  await packageModel.update(pack);
};

export const deletePackage = async ({ disciplineId, owner, id }: packageType) => {
  await packageModel.delete({ disciplineId, owner, id });
};
