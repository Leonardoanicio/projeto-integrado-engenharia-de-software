import { classModel } from 'src/models/classModel';
import type { EntityItem } from 'dynamodb-toolbox';
import { randomUUID } from 'crypto';
import { createAppointment } from './appointmentRepositories';

type classType = EntityItem<typeof classModel>;

export const getClass = async ({ id }) => {
  const queryOptions = { filters: { attr: 'id' as keyof classType, eq: id } };

  let LastEvaluatedKey = null;
  let classes = [];
  do {
    const response = await classModel.query('CLASS', queryOptions);
    LastEvaluatedKey = response.LastEvaluatedKey;
    classes = [...classes, ...(response.Items || [])];
  } while (LastEvaluatedKey);

  if (classes.length === 0) {
    throw new Error('Class not found');
  }

  return classes[0];
};

export const listClasses = async ({ email }) => {
  const queryOptions = { filters: { attr: 'email_aluno' as keyof classType, eq: email } };

  let LastEvaluatedKey = null;
  let classes = [];
  do {
    const response = await classModel.query('CLASS', queryOptions);
    LastEvaluatedKey = response.LastEvaluatedKey;
    classes = [...classes, ...(response.Items || [])];
  } while (LastEvaluatedKey);

  return classes;
};

export const createClass = async ({ email, pack, spot, spotNumber }) => {
  const classToCreate = {
    email_aluno: email,
    disciplineId: pack.disciplineId,
    id: randomUUID(),
    email_professor: pack.owner,
    name: pack.packageName,
    details: pack.details,
    value: pack.value,
    lessons: pack.lessons,
    disciplineName: pack.disciplineName,
    packId: pack.id,
    spotNumber: spotNumber
  };

  await classModel.put(classToCreate);
  await createAppointment(
    classToCreate.id,
    pack.packageName,
    pack.lessons,
    email,
    pack.owner,
    spot
  );
  return classToCreate;
};

export const deleteClass = async ({ id }) => {
  const classToDelete = await getClass({ id });
  await classModel.delete(classToDelete);

  return classToDelete;
};
