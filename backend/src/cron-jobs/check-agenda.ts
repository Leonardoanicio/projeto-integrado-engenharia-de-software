import { sendGridSendEmail } from 'src/api/sendgrid';
import { getTodayAppointments } from 'src/repositories/appointmentRepositories';

export const checkAgenda = async () => {
  try {
    const appointments = await getTodayAppointments();
    console.log(appointments);

    if (!appointments.length) {
      return;
    }

    for (const appointment of appointments) {
      const { aluno, professor, details, beginsAt } = appointment;
      const prettyBeginsAt = new Date(beginsAt).toLocaleString('pt-BR', {
        timeZone: 'America/Sao_Paulo'
      });

      const subject = `Lembrete de aula marcada para ${prettyBeginsAt}`;
      const mensagemProfessor = `Olá professor, você tem uma aula de ${details} marcada para hoje às ${prettyBeginsAt} com o aluno ${aluno}.`;
      const mensagemAluno = `Olá aluno, você tem uma aula de ${details}  marcada para hoje às ${prettyBeginsAt} com o professor ${professor}.`;
      await sendGridSendEmail(aluno, subject, mensagemAluno);
      await sendGridSendEmail(professor, subject, mensagemProfessor);
    }
  } catch (error) {
    console.error(error);
  }
};
