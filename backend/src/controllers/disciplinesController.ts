import { APIGatewayEvent } from 'aws-lambda';
import { randomUUID } from 'crypto';
import { disciplineModel } from 'src/models/disciplineModel';
import {
  createDiscipline,
  deleteDiscipline,
  findDiscipline,
  listUserDisciplines
} from 'src/repositories/disciplinesRepositories';
import { listPackages } from 'src/repositories/packagesRepositories';
import { CreateResponse } from 'src/utils';

export const get = async (event: APIGatewayEvent) => {
  try {
    const { email, id = undefined } = event.queryStringParameters;
    if (id) {
      const discipline = await findDiscipline({ email, id });
      return discipline ? CreateResponse(discipline, 200) : CreateResponse(undefined, 404);
    }

    const disciplines = await listUserDisciplines({ email });
    return CreateResponse(disciplines, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};

export const create = async (event: APIGatewayEvent) => {
  try {
    const body = JSON.parse(event.body);
    const { email, name } = body;
    const discipline = {
      email,
      name,
      id: randomUUID()
    };
    await createDiscipline(discipline);
    return CreateResponse(discipline.id, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};

export const remove = async (event: APIGatewayEvent) => {
  try {
    const { email, id } = event.queryStringParameters;
    const discipline = await findDiscipline({ email, id });
    if (!discipline) {
      return CreateResponse(undefined, 404);
    }

    const packages = await listPackages({
      owner: email,
      disciplineId: id,
      disciplineName: discipline.name
    });
    if (packages.length > 0 && packages.some((pack) => pack.agenda.some((spot) => spot.filled))) {
      return CreateResponse(undefined, 400);
    }
    await deleteDiscipline({ email, id });
    return CreateResponse(discipline.id, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};
