import { APIGatewayEvent } from 'aws-lambda';

import { CreateResponse } from '../utils';
import { revalidatePreSignedUrl, generatePutPreSignedUrl } from '../utils/s3';
import { findUser, createUser, updateUser } from '../repositories/userRepositories';
import { changeUserPassword } from '../utils/cognito';
import { sendGridSendEmail } from 'src/api/sendgrid';

export const get = async (event: APIGatewayEvent) => {
  try {
    const { email = undefined } = event.pathParameters || {};
    if (!email) {
      throw new Error('Chama incorreta');
    }

    const user = await findUser({ email });
    if (!user) {
      return CreateResponse('', 204);
    }

    if (user.profilePicture) {
      user.profilePicture = await revalidatePreSignedUrl(user.profilePicture);
    }

    return CreateResponse(user, 200);
  } catch (err) {
    console.error(err);
    console.log(event);
    return CreateResponse('Usuario não encontrado', 500);
  }
};

export const create = async (event: APIGatewayEvent) => {
  try {
    const { email = undefined } = event.pathParameters || {};
    if (!email) {
      throw new Error('Chama incorreta');
    }

    if (!event.body) {
      throw new Error('Chamada incorreta');
    }
    const parsedBody = JSON.parse(event.body);

    const { userType } = parsedBody;
    if (!userType) {
      return CreateResponse('Campo email e tipo são obrigatórios', 400);
    }

    const existingUser = await findUser({ email });
    if (existingUser) {
      return CreateResponse('O email informado já está sendo utilizado', 400);
    }

    const user = {
      email,
      userType,
      active: true,
      accountDeleted: false
    };
    await createUser(user);

    await sendGridSendEmail(email, 'Bem vindo!', 'Bem vindo ao Pingo!');

    return CreateResponse(user, 201);
  } catch (err) {
    console.error(err);
    console.log(event);
    return CreateResponse('Erro, favor contatar o suporte', 500);
  }
};

export const update = async (event: APIGatewayEvent) => {
  try {
    const { email = undefined } = event.pathParameters || {};
    if (!event.body || !email) {
      throw new Error('Chamada incorreta');
    }

    const { changes } = JSON.parse(event.body);

    const oldUser = await findUser({ email });
    if (!oldUser) {
      throw new Error(`Usuário ${email} não encontrado`);
    }

    await updateUser({
      ...oldUser,
      ...changes
    });

    return CreateResponse('Usuario atualizado', 200);
  } catch (err) {
    console.error(err);
    console.log(event);
    return CreateResponse('Falha ao atualizar o usuario', 500);
  }
};

export const updatePassword = async (event: APIGatewayEvent) => {
  try {
    if (!event.body || !event.headers.Authorization) {
      throw new Error('Chamada incorreta');
    }

    const { email, oldPassword, newPassword, accessToken } = JSON.parse(event.body);
    const userFound = await findUser({ email });
    if (!userFound) {
      return CreateResponse('Usuario não encontrado', 400);
    }

    await changeUserPassword(accessToken, oldPassword, newPassword);

    return CreateResponse('User updated', 200);
  } catch (err) {
    console.error(err);
    console.log(event);
    if ((err as Error).message.includes('Incorrect username or password')) {
      return CreateResponse('Senha incorreta', 500);
    }
    return CreateResponse('Falha ao alterar a senha', 500);
  }
};

export const generatePresignedUrl = async (event: APIGatewayEvent) => {
  try {
    const { email, filename } = event.queryStringParameters || {};
    if (!email || !filename) {
      throw new Error('Chamada incorreta');
    }

    const user = await findUser({ email });
    if (!user) {
      return CreateResponse('User not found', 204);
    }

    const signedURL = await generatePutPreSignedUrl(email, filename);
    await updateUser({
      ...user,
      profilePicture: signedURL
    });

    return CreateResponse(signedURL, 200);
  } catch (err) {
    console.error(err);
    console.log(event);
    return CreateResponse('Falha ao atualizar a foto', 500);
  }
};
