import { APIGatewayEvent } from 'aws-lambda';
import { randomUUID } from 'crypto';
import {
  createPackage,
  deletePackage,
  findPackage,
  listPackages,
  updatePackage
} from 'src/repositories/packagesRepositories';
import { CreateResponse } from 'src/utils';

export const get = async (event: APIGatewayEvent) => {
  try {
    const {
      discipline = undefined,
      disciplineName = undefined,
      email = undefined,
      id = undefined
    } = event.queryStringParameters;
    if (id) {
      const pack = await findPackage({ disciplineId: discipline, owner: email, id });
      return pack ? CreateResponse(pack, 200) : CreateResponse(undefined, 404);
    }

    const packages = await listPackages({ disciplineId: discipline, disciplineName, owner: email });
    return CreateResponse(packages, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};

export const create = async (event: APIGatewayEvent) => {
  try {
    const body = JSON.parse(event.body);
    const { agenda, details, discipline, disciplineName, owner, packageName, lessons, value } =
      body;
    const pack = {
      disciplineId: discipline,
      owner,
      id: randomUUID(),
      packageName,
      details,
      value,
      lessons,
      agenda,
      disciplineName
    };
    await createPackage(pack);
    return CreateResponse({ id: pack.id }, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};

export const update = async (event: APIGatewayEvent) => {
  try {
    const body = JSON.parse(event.body);
    const { agenda, details, discipline, disciplineName, owner, packageName, lessons, value, id } =
      body;
    const packFound = await findPackage({ disciplineId: discipline, owner, id });
    if (!packFound) {
      return CreateResponse('Not Found', 404);
    }
    const pack = {
      disciplineId: discipline,
      owner,
      packageName,
      details,
      value,
      lessons,
      agenda,
      disciplineName,
      id: packFound.id
    };
    await updatePackage(pack);
    return CreateResponse({ id: pack.id }, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};

export const remove = async (event: APIGatewayEvent) => {
  try {
    const { email, id, discipline } = event.queryStringParameters;
    const pack = await findPackage({ owner: email, id, disciplineId: discipline });
    if (!pack) {
      return CreateResponse(undefined, 404);
    }
    await deletePackage({ owner: email, id, disciplineId: discipline });
    return CreateResponse(pack.id, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};
