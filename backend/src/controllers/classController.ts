import { APIGatewayEvent } from 'aws-lambda';
import { deleteAppointment } from 'src/repositories/appointmentRepositories';
import { createClass, deleteClass, listClasses } from 'src/repositories/classesRepositories';
import { findPackage, updatePackage } from 'src/repositories/packagesRepositories';
import { findUser, updateUser } from 'src/repositories/userRepositories';
import { CreateResponse } from 'src/utils';

export const getUserClasses = async (event: APIGatewayEvent) => {
  try {
    const { email } = event.queryStringParameters;

    const user = await findUser({ email });

    if (!user) {
      return CreateResponse('User not found', 404);
    }

    const classes = await listClasses({ email });

    return CreateResponse(classes, 200);
  } catch (error) {
    console.error(error);
    return CreateResponse('Internal server error', 500);
  }
};

export const buy = async (event: APIGatewayEvent) => {
  try {
    if (!event.body || !event.headers.Authorization) {
      throw new Error('Chamada incorreta');
    }

    const { email, pack, selectedSpot } = JSON.parse(event.body);

    const userFound = await findUser({ email });
    if (!userFound) {
      return CreateResponse('Usuário não encontrado', 404);
    }

    const packFound = await findPackage({
      owner: pack.owner,
      id: pack.id,
      disciplineId: pack.disciplineId
    });
    if (!packFound) {
      return CreateResponse('Pacote não encontrado', 404);
    }
    delete packFound.user;

    if (userFound.credits < packFound.value) {
      return CreateResponse('Saldo insuficiente', 400);
    }

    if (packFound.agenda[selectedSpot].filled) {
      return CreateResponse('Vaga já preenchida', 400);
    }

    await updatePackage({
      ...packFound,
      agenda: [
        ...packFound.agenda.slice(0, selectedSpot),
        { ...packFound.agenda[selectedSpot], filled: true },
        ...packFound.agenda.slice(selectedSpot + 1)
      ]
    });

    await createClass({
      email,
      pack: packFound,
      spot: packFound.agenda[selectedSpot],
      spotNumber: selectedSpot
    });

    await updateUser({
      ...userFound,
      credits: userFound.credits - packFound.value
    });

    return CreateResponse(pack.id, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};

export const remove = async (event: APIGatewayEvent) => {
  try {
    const { queryStringParameters } = event;
    const { id } = queryStringParameters;

    const deletedClass = await deleteClass({ id });
    const {
      email_aluno: aluno,
      email_professor: professor,
      id: classId,
      disciplineId,
      packId,
      spotNumber
    } = deletedClass;

    const packFound = await findPackage({
      owner: professor,
      id: packId,
      disciplineId: disciplineId
    });

    if (packFound.user) {
      delete packFound.user;
    }
    await updatePackage({
      ...packFound,
      agenda: [
        ...packFound.agenda.slice(0, spotNumber),
        { ...packFound.agenda[spotNumber], filled: false },
        ...packFound.agenda.slice(spotNumber + 1)
      ]
    });

    await deleteAppointment({ aluno, professor, classId });
    return CreateResponse(id, 200);
  } catch (err) {
    console.error(err);
    return CreateResponse('Internal server error', 500);
  }
};
