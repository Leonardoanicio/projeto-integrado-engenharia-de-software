import { APIGatewayEvent } from 'aws-lambda';
import { getAppointments } from 'src/repositories/appointmentRepositories';
import { findUser } from 'src/repositories/userRepositories';
import { CreateResponse } from 'src/utils';

export const list = async (event: APIGatewayEvent) => {
  try {
    const { email } = event.queryStringParameters;

    const user = await findUser({ email });

    if (!user) {
      return CreateResponse('User not found', 404);
    }

    const appointments = await getAppointments(email);

    return CreateResponse(appointments, 200);
  } catch (error) {
    console.error(error);
    return CreateResponse('Internal server error', 500);
  }
};
