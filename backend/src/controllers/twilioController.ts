import { APIGatewayEvent } from 'aws-lambda';
import { putEvent } from '../repositories/eventRepositories';
import { CreateResponse } from '../utils';
import { parseTwiloTextBodyToObject } from '../utils/api_gateway';
import { generateAccessToken, twilioClient } from '../api/twilio';
import { randomUUID } from 'crypto';
import { putRoom, updateRoomParticipants } from 'src/repositories/roomRepositories';

export const createRoom = async (event: APIGatewayEvent) => {
  try {
    const { roomName, user } = JSON.parse(event.body);
    const roomExists = await twilioClient.video.v1.rooms.list({ uniqueName: roomName });
    if (!roomExists.length) {
      console.log(process.env.TWILIO_STATUS_CALLBACK);
      const { sid } = await twilioClient.video.v1.rooms.create({
        uniqueName: String(roomName),
        type: 'go',
        unusedRoomTimeout: 5,
        audioOnly: false,
        statusCallback: process.env.TWILIO_STATUS_CALLBACK || '',
        statusCallbackMethod: 'POST'
      });

      await putRoom({
        RoomName: roomName,
        RoomSid: sid,
        RoomType: 'go',
        BeginsAt: new Date().toISOString(),
        participants: [user]
      });
    } else {
      await updateRoomParticipants(roomName, user);
    }

    const accessToken = generateAccessToken(roomName, user, 60);

    return CreateResponse(
      {
        roomName,
        accessToken
      },
      200
    );
  } catch (error) {
    console.error(error);
    return CreateResponse('Internal server error', 500);
  }
};

export const logEvent = async (event: APIGatewayEvent) => {
  try {
    const { body } = event;
    console.log('Body', body);
    const {
      RoomName,
      RoomSid,
      RoomType,
      RoomStatus,
      StatusCallbackEvent,
      Timestamp,
      ParticipantSid,
      ParticipantIdentity,
      ParticipantStatus,
      ParticipantDuration,
      RoomDuration
    } = parseTwiloTextBodyToObject(body);
    if (!RoomName || !RoomSid) {
      return CreateResponse('', 204);
    }
    await putEvent({
      RoomName,
      RoomSid,
      RoomType,
      RoomStatus,
      StatusCallbackEvent,
      Timestamp,
      ParticipantSid,
      ParticipantIdentity,
      ParticipantStatus,
      ParticipantDuration: ParticipantDuration ? parseInt(ParticipantDuration) : undefined,
      RoomDuration: RoomDuration ? parseInt(RoomDuration) : undefined,
      id: randomUUID().toString()
    });
    return CreateResponse('', 204);
  } catch (error) {
    console.error(error);
    return CreateResponse('Internal server error', 500);
  }
};
