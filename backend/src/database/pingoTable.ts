import { Table } from 'dynamodb-toolbox';
import { DynamoDB } from 'aws-sdk';

if (!process.env.DYNAMODB_TABLE_NAME) {
  throw new Error('Missing DYNAMODB_TABLE_NAME in env');
}

const DocumentClient = new DynamoDB.DocumentClient();
export const MainTable = new Table({
  name: process.env.DYNAMODB_TABLE_NAME,
  partitionKey: 'pk',
  sortKey: 'sk',
  DocumentClient
});
