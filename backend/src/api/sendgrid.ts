import sgMail from '@sendgrid/mail';
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

export const sendGridSendEmail = async (to: string, subject: string, text: string) => {
  const msg = {
    from: '"Pingo" <donotreply.pingo@gmail.com>',
    subject,
    text,
    to
  };
  return sgMail
    .send(msg)
    .then(() => {
      console.log('Email sent');
    })
    .catch((error) => {
      console.error(error);
    });
};
