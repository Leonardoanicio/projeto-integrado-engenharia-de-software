import { Twilio, jwt } from 'twilio';

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const apiKey = process.env.TWILIO_API_KEY;
const apiSecret = process.env.TWILIO_API_SECRET;

export const twilioClient = new Twilio(accountSid, authToken);

export const generateAccessToken = (roomIdentifier: string, identity: string, ttl: number) => {
  const AccessToken = jwt.AccessToken;
  const VideoGrant = AccessToken.VideoGrant;

  const videoGrand = new VideoGrant({
    room: roomIdentifier
  });

  const token = new AccessToken(accountSid, apiKey, apiSecret, { identity, ttl });
  token.addGrant(videoGrand);

  return token.toJwt();
};
