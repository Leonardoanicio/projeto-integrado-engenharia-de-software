import { S3 } from 'aws-sdk';

export const revalidatePreSignedUrl = async (link: string) => {
  const url = new URL(link);
  const bucket = process.env.USERS_BUCKET;
  const fileKey = decodeURIComponent(url.pathname).slice(1);

  const s3Client = new S3();
  return s3Client.getSignedUrl('getObject', {
    Bucket: bucket,
    Key: fileKey,
    Expires: 60 * 60 * 24
  });
};

export const generatePutPreSignedUrl = async (email: string, filename: string) => {
  const s3Client = new S3();
  return s3Client.getSignedUrl('putObject', {
    Bucket: process.env.USERS_BUCKET,
    Key: `${email}/profile_picture/${filename}`,
    Expires: 60 * 60 * 24
  });
};
