import { CognitoIdentityServiceProvider } from 'aws-sdk';

export const changeUserPassword = async (
  accessToken: string,
  oldPassword: string,
  newPassword: string
) => {
  const cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
  return cognitoidentityserviceprovider
    .changePassword({
      AccessToken: accessToken,
      PreviousPassword: oldPassword,
      ProposedPassword: newPassword
    })
    .promise();
};
