type responseType =
  | string
  | number
  | boolean
  | undefined
  | responseType[]
  | { [key: string]: responseType };

export const CreateResponse = async (response: responseType, status_code: number) => {
  return {
    statusCode: status_code,
    body: JSON.stringify(response),
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    }
  };
};

export const parseTwiloTextBodyToObject = (body: string) => {
  const bodyObject = body.split('&').reduce((accumulator, current) => {
    const [key, value] = current.split('=');
    accumulator[key] = decodeURI(value).replace(/%2B/g, '+');
    return accumulator;
  }, {} as { [key: string]: string });
  return bodyObject;
};
