import { APIGatewayEvent, EventBridgeEvent } from 'aws-lambda';
import { randomUUID } from 'crypto';
import jwtDecode from 'jwt-decode';
import { putLog } from 'src/repositories/logRepositories';

export const log = async (
  event: APIGatewayEvent | EventBridgeEvent<'Scheduled Event', 'Scheduled Event'>
) => {
  try {
    if ((event as EventBridgeEvent<'Scheduled Event', 'Scheduled Event'>).source === 'aws.events')
      return;

    const {
      headers: { Authorization },
      path
    } = event as APIGatewayEvent;
    if (path === '/users' || path === '/event') return;

    if (!Authorization) {
      throw new Error('No token provided');
    }

    const payload: any = jwtDecode(Authorization);
    const { email } = payload;

    await putLog({
      user: email,
      token: Authorization,
      date: new Date().toISOString(),
      payload: JSON.stringify(event),
      endpoint: path,
      id: randomUUID()
    });
    return true;
  } catch (error) {
    console.error(error);
  }
};
