Como executar:
    Frontend:
        - yarn install
        - yarn dev (para rodar com backend em dev)
        - yarn prod (para rodar com backend em prod)
        - yarn local (para rodar com backend local)
            - é necessário criar um arquvio .env.local com as configurações do seu backend local
            - para isto, altere a variável REACT_APP_BASE_URL para o endereço do seu backend local,
              e as variaveis REACT_APP_COGNITO_POOL_ID e REACT_APP_COGNITO_CLIENT_ID para os valores
              da sua pool de usuários Cognito 

        O deploy do frontend é feito automaticamente pelo amplify quando o repo é atualizado.
    Backend:
        Para rodar o backend localmente é necessário ter contas na AWS, Twilio e Sendgrid.  

        Twilio:
            Utilizado para realizar as videochamadas. É necessário criar uma conta e um projeto no Twilio.
            São necessário as seguintes variáveis de ambiente:
                - TWILIO_ACCOUNT_SID: ID da conta Twilio
                - TWILIO_AUTH_TOKEN: Token da conta Twilio
                - TWILIO_API_KEY: API Key da conta Twilio
                - TWILIO_API_SECRET: API Key Secret da conta Twilio
            Um resumo de como obter estas variáveis pode ser encontrado em:
                https://www.twilio.com/docs/glossary/what-is-an-api-key
                https://support.twilio.com/hc/en-us/articles/223136027-Auth-Tokens-and-How-to-Change-Them
        
        SendGrid:
            Utilizado para enviar os emails de confirmação de cadastro e lembretes de aulas. 
            É necessário criar uma conta e um projeto no SendGrid.
            São necessário as seguintes variáveis de ambiente:
                - SENDGRID_API_KEY: API Key da conta SendGrid
            Um resumo de como obter estas variáveis pode ser encontrado em:
                https://docs.sendgrid.com/ui/account-and-settings/api-keys

        AWS: 
            Contém a infraestrutura do backend. É necessário criar uma conta na AWS e configurar o
            AWS CLI com as credenciais da conta (para rodar localmente).
            
            Sugere-se que as variáveis de ambiente sejam guardadas no parameter store da AWS, como
            visto no arquivo serverless/env.yml.
        
        Após as variáveis configuradas, para rodar o backend localmente:
            - altere o arquivo package.json para que o script "dev" utilize o AWS_PROFILE correto
            - yarn install
            - yarn dev
        
        Para realizar o deploy do backend:
            - yarn sls deploy --verbose --stage stage_name